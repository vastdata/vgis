--- build a larger database
\i :regdir/core/regress_lots_of_points.sql
-- Generate 50000 deterministic pseudorandom points
CREATE TABLE "test" (
	"num" bigint,
	"the_geom" geometry
);
CREATE TABLE
INSERT INTO "test" ("num", "the_geom")
  SELECT path[1], ST_AsText(geom, 6)
  FROM (
    SELECT (ST_DumpPoints(ST_GeneratePoints(ST_MakeEnvelope(0, 0, 1000, 1000), 50000, 654321))).*
  ) f;
INSERT 0 50000
--- test some of the searching capabilities
CREATE OR REPLACE FUNCTION qnodes(q text) RETURNS text
LANGUAGE 'plpgsql' AS
$$
DECLARE
  exp TEXT;
  mat TEXT[];
  ret TEXT[];
BEGIN
  FOR exp IN EXECUTE 'EXPLAIN ' || q
  LOOP
    --RAISE NOTICE 'EXP: %', exp;
    mat := regexp_matches(exp, ' *(?:-> *)?(.*Scan)');
    --RAISE NOTICE 'MAT: %', mat;
    IF mat IS NOT NULL THEN
      ret := array_append(ret, mat[1]);
    END IF;
    --RAISE NOTICE 'RET: %', ret;
  END LOOP;
  RETURN array_to_string(ret,',');
END;
$$;
CREATE FUNCTION
-- GiST index
CREATE INDEX quick_gist on test using gist (the_geom);
CREATE INDEX
set enable_indexscan = off;
SET
set enable_bitmapscan = off;
SET
set enable_seqscan = on;
SET
SELECT 'scan_idx', qnodes('select * from test where the_geom && ST_MakePoint(0,0)');
 ?column? |  qnodes  
----------+----------
 scan_idx | Seq Scan
(1 row)

 select num,ST_astext(the_geom) from test where the_geom && 'BOX3D(125 125,135 135)'::box3d order by num;
  num  |          st_astext           
-------+------------------------------
  5323 | POINT(134.204197 130.674106)
 11208 | POINT(126.522745 128.356924)
 27373 | POINT(129.481991 125.755641)
 33863 | POINT(127.143785 131.932614)
 45851 | POINT(130.986464 132.890625)
(5 rows)

set enable_indexscan = on;
SET
set enable_bitmapscan = off;
SET
set enable_seqscan = off;
SET
SELECT 'scan_seq', qnodes('select * from test where the_geom && ST_MakePoint(0,0)');
 ?column? |   qnodes   
----------+------------
 scan_seq | Index Scan
(1 row)

 select num,ST_astext(the_geom) from test where the_geom && 'BOX3D(125 125,135 135)'::box3d order by num;
  num  |          st_astext           
-------+------------------------------
  5323 | POINT(134.204197 130.674106)
 11208 | POINT(126.522745 128.356924)
 27373 | POINT(129.481991 125.755641)
 33863 | POINT(127.143785 131.932614)
 45851 | POINT(130.986464 132.890625)
(5 rows)

CREATE FUNCTION estimate_error(qry text, tol int)
RETURNS text
LANGUAGE 'plpgsql' VOLATILE AS $$
DECLARE
  anl TEXT := ''; -- analisys
  err INT; -- absolute difference between planned and actual rows
  est INT; -- estimated count
  act INT; -- actual count
  mat TEXT[];
  anl_tmp TEXT;
  r record;
BEGIN
  for r in  EXECUTE 'EXPLAIN ANALYZE ' || qry
  LOOP
      anl_tmp = r;
      anl = anl || anl_tmp;
  end loop;
  SELECT regexp_matches(anl, ' rows=([0-9]*) .* rows=([0-9]*) ')
  INTO mat;

  est := mat[1];
  act := mat[2];

  err = abs(est-act);

  RETURN act || '+=' || tol || ':' || coalesce(
    nullif((err < tol)::text,'false'),
    'false:'||err::text
    );

END;
$$;
CREATE FUNCTION
-- There are 50000 points in the table with full extent being
-- BOX(0.001693 0.000185,999.968899 999.997026)
CREATE TABLE sample_queries AS
SELECT 1 as id, 5 as tol, 'ST_MakeEnvelope(125,125,135,135)' as box
 UNION ALL
SELECT 2, 60, 'ST_MakeEnvelope(0,0,135,135)'
 UNION ALL
SELECT 3, 500, 'ST_MakeEnvelope(0,0,500,500)'
 UNION ALL
SELECT 4, 600, 'ST_MakeEnvelope(0,0,1000,1000)'
;
INSERT 0 4
-- We raise the statistics target to the limit
ALTER TABLE test ALTER COLUMN the_geom SET STATISTICS 10000;
ALTER TABLE
ANALYZE test;
ANALYZE
SELECT estimate_error(
  'select num from test where the_geom && ' || box, tol )
  FROM sample_queries ORDER BY id;
 estimate_error 
----------------
 5+=5:1
 907+=60:1
 12505+=500:1
 50000+=600:1
(4 rows)

-- Test selectivity estimation of functional indexes
CREATE INDEX expressional_gist on test using gist ( st_centroid(the_geom) );
CREATE INDEX
ANALYZE test;
ANALYZE
SELECT 'expr', estimate_error(
  'select num from test where st_centroid(the_geom) && ' || box, tol )
  FROM sample_queries ORDER BY id;
 ?column? | estimate_error 
----------+----------------
 expr     | 5+=5:1
 expr     | 907+=60:1
 expr     | 12505+=500:1
 expr     | 50000+=600:1
(4 rows)

DROP TABLE test;
DROP TABLE
DROP TABLE sample_queries;
DROP TABLE
DROP FUNCTION estimate_error(text, int);
DROP FUNCTION
DROP FUNCTION qnodes(text);
DROP FUNCTION
set enable_indexscan = on;
SET
set enable_bitmapscan = on;
SET
set enable_seqscan = on;
SET
-- _ST_SortableHash is a work around Postgres parallel sort requiring recalculation of abbreviated keys.
select '_st_sortablehash', _ST_SortableHash('POINT(0 0)'), _ST_SortableHash('SRID=4326;POINT(0 0)'), _ST_SortableHash('SRID=3857;POINT(0 0)');
     ?column?     | _st_sortablehash |  _st_sortablehash  |  _st_sortablehash  
------------------+------------------+--------------------+--------------------
 _st_sortablehash |                0 | 768602608280535040 | 768602608280535040
(1 row)

