-- Point
SELECT 'pnt1', ST_AsText(ST_Reverse('POINT EMPTY'::geometry));
 ?column? |  st_astext  
----------+-------------
 pnt1     | POINT EMPTY
(1 row)

SELECT 'pnt2', ST_AsText(ST_Reverse('POINT(0 0)'::geometry));
 ?column? | st_astext  
----------+------------
 pnt2     | POINT(0 0)
(1 row)

-- Multipoint
SELECT 'mpnt1', ST_AsText(ST_Reverse('MULTIPOINT EMPTY'::geometry));
 ?column? |    st_astext     
----------+------------------
 mpnt1    | MULTIPOINT EMPTY
(1 row)

SELECT 'mpnt2', ST_AsText(ST_Reverse('MULTIPOINT(0 0)'::geometry));
 ?column? |    st_astext    
----------+-----------------
 mpnt2    | MULTIPOINT(0 0)
(1 row)

SELECT 'mpnt3', ST_AsText(ST_Reverse('MULTIPOINT(0 0,1 1)'::geometry));
 ?column? |      st_astext      
----------+---------------------
 mpnt3    | MULTIPOINT(0 0,1 1)
(1 row)

-- Linestring
SELECT 'lin1', ST_AsText(ST_Reverse('LINESTRING EMPTY'::geometry));
 ?column? |    st_astext     
----------+------------------
 lin1     | LINESTRING EMPTY
(1 row)

SELECT 'lin2', ST_AsText(ST_Reverse('LINESTRING(0 0, 1 1)'::geometry));
 ?column? |      st_astext      
----------+---------------------
 lin2     | LINESTRING(1 1,0 0)
(1 row)

SELECT 'lin2m', ST_AsText(ST_Reverse('LINESTRING M (0 0 3,1 1 6)'::geometry));
 ?column? |         st_astext          
----------+----------------------------
 lin2m    | LINESTRING M (1 1 6,0 0 3)
(1 row)

SELECT 'lin2z', ST_AsText(ST_Reverse('LINESTRING Z (0 0 3,1 1 6)'::geometry));
 ?column? |         st_astext          
----------+----------------------------
 lin2z    | LINESTRING Z (1 1 6,0 0 3)
(1 row)

-- MultiLinestring
SELECT 'mlin1', ST_AsText(ST_Reverse('MULTILINESTRING EMPTY'::geometry));
 ?column? |       st_astext       
----------+-----------------------
 mlin1    | MULTILINESTRING EMPTY
(1 row)

SELECT 'mlin2', ST_AsText(ST_Reverse('MULTILINESTRING((0 0, 1 0),(2 0, -3 3))'::geometry));
 ?column? |               st_astext               
----------+---------------------------------------
 mlin2    | MULTILINESTRING((1 0,0 0),(-3 3,2 0))
(1 row)

SELECT 'mlin2m', ST_AsText(ST_Reverse('MULTILINESTRING M ((0 0 5, 1 0 3),(2 0 2, -3 3 1))'::geometry));
 ?column? |                    st_astext                     
----------+--------------------------------------------------
 mlin2m   | MULTILINESTRING M ((1 0 3,0 0 5),(-3 3 1,2 0 2))
(1 row)

SELECT 'mlin2z', ST_AsText(ST_Reverse('MULTILINESTRING Z ((0 0 5, 1 0 3),(2 0 2, -3 3 1))'::geometry));
 ?column? |                    st_astext                     
----------+--------------------------------------------------
 mlin2z   | MULTILINESTRING Z ((1 0 3,0 0 5),(-3 3 1,2 0 2))
(1 row)

-- Polygon
SELECT 'plg1', ST_AsText(ST_Reverse('POLYGON EMPTY'::geometry));
 ?column? |   st_astext   
----------+---------------
 plg1     | POLYGON EMPTY
(1 row)

SELECT 'plg2', ST_AsText(ST_Reverse('POLYGON((0 0,8 0,8 8,0 8,0 0),(2 2,2 4,4 4,4 2,2 2))'::geometry));
 ?column? |                      st_astext                       
----------+------------------------------------------------------
 plg2     | POLYGON((0 0,0 8,8 8,8 0,0 0),(2 2,4 2,4 4,2 4,2 2))
(1 row)

SELECT 'plg2m', ST_AsText(ST_Reverse('POLYGON M ((0 0 9,8 0 8,8 8 7,0 8 8,0 0 9),(2 2 1,2 4 2,4 4 3,4 2 2,2 2 1))'::geometry));
 ?column? |                                  st_astext                                  
----------+-----------------------------------------------------------------------------
 plg2m    | POLYGON M ((0 0 9,0 8 8,8 8 7,8 0 8,0 0 9),(2 2 1,4 2 2,4 4 3,2 4 2,2 2 1))
(1 row)

SELECT 'plg2z', ST_AsText(ST_Reverse('POLYGON Z ((0 0 9,8 0 8,8 8 7,0 8 8,0 0 9),(2 2 1,2 4 2,4 4 3,4 2 2,2 2 1))'::geometry));
 ?column? |                                  st_astext                                  
----------+-----------------------------------------------------------------------------
 plg2z    | POLYGON Z ((0 0 9,0 8 8,8 8 7,8 0 8,0 0 9),(2 2 1,4 2 2,4 4 3,2 4 2,2 2 1))
(1 row)

-- MultiPolygon
SELECT 'mplg1', ST_AsText(ST_Reverse('MULTIPOLYGON EMPTY'::geometry));
 ?column? |     st_astext      
----------+--------------------
 mplg1    | MULTIPOLYGON EMPTY
(1 row)

SELECT 'mplg2', ST_AsText(ST_Reverse('MULTIPOLYGON(((0 0,5 0,5 5,0 5,0 0),(2 2,2 4,4 4,4 2,2 2)),((6 6,7 6,7 7,6 6)))'::geometry));
 ?column? |                                    st_astext                                    
----------+---------------------------------------------------------------------------------
 mplg2    | MULTIPOLYGON(((0 0,0 5,5 5,5 0,0 0),(2 2,4 2,4 4,2 4,2 2)),((6 6,7 7,7 6,6 6)))
(1 row)

SELECT 'mplg2m', ST_AsText(ST_Reverse('MULTIPOLYGON M (((0 0 1,5 0 2,5 5 3,0 5 4,0 0 1),(2 2 0,2 4 1,4 4 2,4 2 3,2 2 0)),((6 6 2,7 6 4,7 7 6,6 6 2)))'::geometry));
 ?column? |                                                   st_astext                                                    
----------+----------------------------------------------------------------------------------------------------------------
 mplg2m   | MULTIPOLYGON M (((0 0 1,0 5 4,5 5 3,5 0 2,0 0 1),(2 2 0,4 2 3,4 4 2,2 4 1,2 2 0)),((6 6 2,7 7 6,7 6 4,6 6 2)))
(1 row)

SELECT 'mplg2z', ST_AsText(ST_Reverse('MULTIPOLYGON Z (((0 0 1,5 0 2,5 5 3,0 5 4,0 0 1),(2 2 0,2 4 1,4 4 2,4 2 3,2 2 0)),((6 6 2,7 6 4,7 7 6,6 6 2)))'::geometry));
 ?column? |                                                   st_astext                                                    
----------+----------------------------------------------------------------------------------------------------------------
 mplg2z   | MULTIPOLYGON Z (((0 0 1,0 5 4,5 5 3,5 0 2,0 0 1),(2 2 0,4 2 3,4 4 2,2 4 1,2 2 0)),((6 6 2,7 7 6,7 6 4,6 6 2)))
(1 row)

-- CircularString
SELECT 'cstr1', ST_AsText(ST_Reverse('CIRCULARSTRING EMPTY'::geometry));
 ?column? |      st_astext       
----------+----------------------
 cstr1    | CIRCULARSTRING EMPTY
(1 row)

SELECT 'cstr2', ST_AsText(ST_Reverse('CIRCULARSTRING(0 0,1 1,2 0,3 -1,4 0)'::geometry));
 ?column? |              st_astext               
----------+--------------------------------------
 cstr2    | CIRCULARSTRING(4 0,3 -1,2 0,1 1,0 0)
(1 row)

SELECT 'cstr2m', ST_AsText(ST_Reverse('CIRCULARSTRING M (0 0 1,1 1 2,2 0 3,3 -1 4,4 0 5)'::geometry));
 ?column? |                     st_astext                     
----------+---------------------------------------------------
 cstr2m   | CIRCULARSTRING M (4 0 5,3 -1 4,2 0 3,1 1 2,0 0 1)
(1 row)

SELECT 'cstr2z', ST_AsText(ST_Reverse('CIRCULARSTRING Z (0 0 1,1 1 2,2 0 3,3 -1 4,4 0 5)'::geometry));
 ?column? |                     st_astext                     
----------+---------------------------------------------------
 cstr2z   | CIRCULARSTRING Z (4 0 5,3 -1 4,2 0 3,1 1 2,0 0 1)
(1 row)

-- CompoundCurve
SELECT 'ccrv1', ST_AsText(ST_Reverse('COMPOUNDCURVE EMPTY'::geometry));
 ?column? |      st_astext      
----------+---------------------
 ccrv1    | COMPOUNDCURVE EMPTY
(1 row)

SELECT 'ccrv2', ST_AsText(ST_Reverse('COMPOUNDCURVE(CIRCULARSTRING(0 0,1 1,1 0),(1 0,0 1))'::geometry));
 ?column? |                      st_astext                       
----------+------------------------------------------------------
 ccrv2    | COMPOUNDCURVE(CIRCULARSTRING(1 0,1 1,0 0),(0 1,1 0))
(1 row)

SELECT 'ccrv2m', ST_AsText(ST_Reverse('COMPOUNDCURVE M (CIRCULARSTRING(0 0 3,1 1 2,1 0 1),(1 0 6,0 1 7))'::geometry));
 ?column? |                              st_astext                               
----------+----------------------------------------------------------------------
 ccrv2m   | COMPOUNDCURVE M (CIRCULARSTRING M (1 0 1,1 1 2,0 0 3),(0 1 7,1 0 6))
(1 row)

SELECT 'ccrv2z', ST_AsText(ST_Reverse('COMPOUNDCURVE Z (CIRCULARSTRING(0 0 3,1 1 2,1 0 1),(1 0 6,0 1 7))'::geometry));
 ?column? |                              st_astext                               
----------+----------------------------------------------------------------------
 ccrv2z   | COMPOUNDCURVE Z (CIRCULARSTRING Z (1 0 1,1 1 2,0 0 3),(0 1 7,1 0 6))
(1 row)

-- CurvePolygon
SELECT 'cplg1', ST_AsText(ST_Reverse('CURVEPOLYGON EMPTY'::geometry));
 ?column? |     st_astext      
----------+--------------------
 cplg1    | CURVEPOLYGON EMPTY
(1 row)

SELECT 'cplg2', ST_AsText(ST_Reverse('CURVEPOLYGON (COMPOUNDCURVE (CIRCULARSTRING (0 0,1 1,1 0),(1 0,0 1),(0 1,0 0)))'::geometry));
 ?column? |                                  st_astext                                   
----------+------------------------------------------------------------------------------
 cplg2    | CURVEPOLYGON(COMPOUNDCURVE(CIRCULARSTRING(1 0,1 1,0 0),(0 1,1 0),(0 0,0 1)))
(1 row)

SELECT 'cplg2m', ST_AsText(ST_Reverse('CURVEPOLYGON M (COMPOUNDCURVE (CIRCULARSTRING (0 0 2,1 1 2,1 0 2),(1 0 2,0 1 2),(0 1 2, 0 0 2)))'::geometry));
 ?column? |                                              st_astext                                              
----------+-----------------------------------------------------------------------------------------------------
 cplg2m   | CURVEPOLYGON M (COMPOUNDCURVE M (CIRCULARSTRING M (1 0 2,1 1 2,0 0 2),(0 1 2,1 0 2),(0 0 2,0 1 2)))
(1 row)

SELECT 'cplg2z', ST_AsText(ST_Reverse('CURVEPOLYGON Z (COMPOUNDCURVE (CIRCULARSTRING (0 0 2,1 1 2,1 0 2),(1 0 2,0 1 2),(0 1 2, 0 0 2)))'::geometry));
 ?column? |                                              st_astext                                              
----------+-----------------------------------------------------------------------------------------------------
 cplg2z   | CURVEPOLYGON Z (COMPOUNDCURVE Z (CIRCULARSTRING Z (1 0 2,1 1 2,0 0 2),(0 1 2,1 0 2),(0 0 2,0 1 2)))
(1 row)

-- MultiCurve
SELECT 'mc1', ST_AsText(ST_Reverse('MULTICURVE EMPTY'::geometry));
 ?column? |    st_astext     
----------+------------------
 mc1      | MULTICURVE EMPTY
(1 row)

SELECT 'mc2', ST_AsText(ST_Reverse('MULTICURVE ((5 5, 3 5, 3 3, 0 3), CIRCULARSTRING (0 0, 0.2 1, 0.5 1.4), COMPOUNDCURVE (CIRCULARSTRING (0 0,1 1,1 0),(1 0,0 1)))'::geometry));
 ?column? |                                                      st_astext                                                       
----------+----------------------------------------------------------------------------------------------------------------------
 mc2      | MULTICURVE((0 3,3 3,3 5,5 5),CIRCULARSTRING(0.5 1.4,0.2 1,0 0),COMPOUNDCURVE(CIRCULARSTRING(1 0,1 1,0 0),(0 1,1 0)))
(1 row)

SELECT 'mc2zm', ST_AsText(ST_Reverse('MULTICURVE ZM ((5 5 0 1, 3 5 2 3, 3 3 7 5, 0 3 9 8), CIRCULARSTRING (0 0 1 3, 0.2 1 4 2, 0.5 1.4 5 1), COMPOUNDCURVE (CIRCULARSTRING (0 0 -1 -2,1 1 0 4,1 0 5 6),(1 0 4 2,0 1 3 4)))'::geometry));
 ?column? |                                                                                       st_astext                                                                                        
----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 mc2zm    | MULTICURVE ZM ((0 3 9 8,3 3 7 5,3 5 2 3,5 5 0 1),CIRCULARSTRING ZM (0.5 1.4 5 1,0.2 1 4 2,0 0 1 3),COMPOUNDCURVE ZM (CIRCULARSTRING ZM (1 0 5 6,1 1 0 4,0 0 -1 -2),(0 1 3 4,1 0 4 2)))
(1 row)

-- MultiSurface
SELECT 'ms1', ST_AsText(ST_Reverse('MULTISURFACE EMPTY'::geometry));
 ?column? |     st_astext      
----------+--------------------
 ms1      | MULTISURFACE EMPTY
(1 row)

SELECT 'ms2', ST_AsText(ST_Reverse('MULTISURFACE (CURVEPOLYGON (CIRCULARSTRING (-2 0, -1 -1, 0 0, 1 -1, 2 0, 0 2, -2 0), (-1 0, 0 0.5, 1 0, 0 1, -1 0)), ((7 8, 10 10, 6 14, 4 11, 7 8)))'::geometry));
 ?column? |                                                             st_astext                                                              
----------+------------------------------------------------------------------------------------------------------------------------------------
 ms2      | MULTISURFACE(CURVEPOLYGON(CIRCULARSTRING(-2 0,0 2,2 0,1 -1,0 0,-1 -1,-2 0),(-1 0,0 1,1 0,0 0.5,-1 0)),((7 8,4 11,6 14,10 10,7 8)))
(1 row)

SELECT 'ms2m', ST_AsText(ST_Reverse(
'MULTISURFACE M (CURVEPOLYGON M (CIRCULARSTRING M (-2 0 0, -1 -1 1, 0 0 2, 1 -1 3, 2 0 4, 0 2 2, -2 0 0), (-1 0 1, 0 0.5 2, 1 0 3, 0 1 3, -1 0 1)), ((7 8 7, 10 10 5, 6 14 3, 4 11 4, 7 8 7)))'
::geometry));
 ?column? |                                                                                   st_astext                                                                                   
----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 ms2m     | MULTISURFACE M (CURVEPOLYGON M (CIRCULARSTRING M (-2 0 0,0 2 2,2 0 4,1 -1 3,0 0 2,-1 -1 1,-2 0 0),(-1 0 1,0 1 3,1 0 3,0 0.5 2,-1 0 1)),((7 8 7,4 11 4,6 14 3,10 10 5,7 8 7)))
(1 row)

SELECT 'ms2z', ST_AsText(ST_Reverse(
'MULTISURFACE Z (CURVEPOLYGON Z (CIRCULARSTRING Z (-2 0 0, -1 -1 1, 0 0 2, 1 -1 3, 2 0 4, 0 2 2, -2 0 0), (-1 0 1, 0 0.5 2, 1 0 3, 0 1 3, -1 0 1)), ((7 8 7, 10 10 5, 6 14 3, 4 11 4, 7 8 7)))'
::geometry));
 ?column? |                                                                                   st_astext                                                                                   
----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 ms2z     | MULTISURFACE Z (CURVEPOLYGON Z (CIRCULARSTRING Z (-2 0 0,0 2 2,2 0 4,1 -1 3,0 0 2,-1 -1 1,-2 0 0),(-1 0 1,0 1 3,1 0 3,0 0.5 2,-1 0 1)),((7 8 7,4 11 4,6 14 3,10 10 5,7 8 7)))
(1 row)

-- PolyedralSurface (TODO)
SELECT 'ps1', ST_AsText(ST_Reverse(
'POLYHEDRALSURFACE EMPTY'
::geometry));
 ?column? |        st_astext        
----------+-------------------------
 ps1      | POLYHEDRALSURFACE EMPTY
(1 row)

SELECT 'ps2', ST_AsText(ST_Reverse(
'POLYHEDRALSURFACE (((0 0,0 0,0 1,0 0)),((0 0,0 1,1 0,0 0)),((0 0,1 0,0 0,0 0)),((1 0,0 1,0 0,1 0)))'
::geometry));
 ?column? |                                             st_astext                                              
----------+----------------------------------------------------------------------------------------------------
 ps2      | POLYHEDRALSURFACE(((0 0,0 1,0 0,0 0)),((0 0,1 0,0 1,0 0)),((0 0,0 0,1 0,0 0)),((1 0,0 0,0 1,1 0)))
(1 row)

-- Triangle
SELECT 'tri1', ST_AsText(ST_Reverse(
'TRIANGLE EMPTY'
::geometry));
 ?column? |   st_astext    
----------+----------------
 tri1     | TRIANGLE EMPTY
(1 row)

SELECT 'tri2', ST_AsText(ST_Reverse(
'TRIANGLE ((1 2,4 5,7 8,1 2))'
::geometry));
 ?column? |          st_astext          
----------+-----------------------------
 tri2     | TRIANGLE((1 2,7 8,4 5,1 2))
(1 row)

SELECT 'tri2z', ST_AsText(ST_Reverse(
'TRIANGLE Z ((1 2 3,4 5 6,7 8 9,1 2 3))'
::geometry));
 ?column? |               st_astext                
----------+----------------------------------------
 tri2z    | TRIANGLE Z ((1 2 3,7 8 9,4 5 6,1 2 3))
(1 row)

-- TIN
SELECT 'tin1', ST_AsText(ST_Reverse( 'TIN EMPTY' ::geometry));
 ?column? | st_astext 
----------+-----------
 tin1     | TIN EMPTY
(1 row)

SELECT 'tin2', ST_AsText(ST_Reverse(
'TIN ( ((0 0, 0 0, 0 1, 0 0)), ((0 0, 0 1, 1 1, 0 0)) )'
::geometry));
 ?column? |                  st_astext                   
----------+----------------------------------------------
 tin2     | TIN(((0 0,0 1,0 0,0 0)),((0 0,1 1,0 1,0 0)))
(1 row)

-- GeometryCollection
SELECT 'gc1', ST_AsText(ST_Reverse(
'GEOMETRYCOLLECTION EMPTY'
::geometry));
 ?column? |        st_astext         
----------+--------------------------
 gc1      | GEOMETRYCOLLECTION EMPTY
(1 row)

SELECT 'gc2', ST_AsText(ST_Reverse(
'GEOMETRYCOLLECTION(MULTIPOLYGON(((0 0,10 0,10 10,0 10,0 0),(2 2,2 5,5 5,5 2,2 2))),POINT(0 0),MULTILINESTRING((0 0, 2 0),(1 1, 2 2)))'
::geometry));
 ?column? |                                                              st_astext                                                              
----------+-------------------------------------------------------------------------------------------------------------------------------------
 gc2      | GEOMETRYCOLLECTION(MULTIPOLYGON(((0 0,0 10,10 10,10 0,0 0),(2 2,5 2,5 5,2 5,2 2))),POINT(0 0),MULTILINESTRING((2 0,0 0),(2 2,1 1)))
(1 row)

