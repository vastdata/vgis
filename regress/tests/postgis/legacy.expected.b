--
-- These tests serve the purpose of ensuring compatibility with
-- old versions of postgis users.
--
-- Their use rely on loading the legacy.sql script.
-- This file also serves as a testcase for uninstall_legacy.sql
--
SET client_min_messages TO WARNING;
SET
\cd :scriptdir
--\i legacy.sql
\! gsql -q postgis_reg -f legacy.sql > /dev/null 2>&1
SELECT 'Starting up MapServer/Geoserver tests...';
                 ?column?                 
------------------------------------------
 Starting up MapServer/Geoserver tests...
(1 row)

-- Set up the data table
SELECT 'Setting up the data table...';
           ?column?           
------------------------------
 Setting up the data table...
(1 row)

CREATE TABLE wmstest ( id INTEGER );
CREATE TABLE
SELECT AddGeometryColumn( 'wmstest', 'pt', 4326, 'POLYGON', 2 );
                addgeometrycolumn                 
--------------------------------------------------
 public.wmstest.pt SRID:4326 TYPE:POLYGON DIMS:2 
(1 row)

INSERT INTO wmstest SELECT lon * 100 + lat AS id, st_setsrid(st_buffer(st_makepoint(lon, lat),1.0),4326) AS pt
FROM (select lon, generate_series(-80,80, 5) AS lat FROM (SELECT generate_series(-175, 175, 5) AS lon) AS sq1) AS sq2;
INSERT 0 2343
ALTER TABLE wmstest add PRIMARY KEY ( id );
ALTER TABLE
CREATE INDEX wmstest_geomidx ON wmstest using gist ( pt );
CREATE INDEX
-- Geoserver 2.0 NG tests
SELECT 'Running Geoserver 2.0 NG tests...';
             ?column?              
-----------------------------------
 Running Geoserver 2.0 NG tests...
(1 row)

-- Run a Geoserver 2.0 NG metadata query
SELECT 'Geoserver1', TYPE FROM geometry_columns WHERE F_TABLE_SCHEMA = 'public' AND F_TABLE_NAME = 'wmstest' AND F_GEOMETRY_COLUMN = 'pt';
  ?column?  |  TYPE   
------------+---------
 Geoserver1 | POLYGON
(1 row)

SELECT 'Geoserver2', SRID FROM geometry_columns WHERE F_TABLE_SCHEMA = 'public' AND F_TABLE_NAME = 'wmstest' AND F_GEOMETRY_COLUMN = 'pt';
  ?column?  | SRID 
------------+------
 Geoserver2 | 4326
(1 row)

-- Run a Geoserver 2.0 NG WMS query -- 2215之后mysql兼容性不支持language sql，legacy.sql脚本中包含大量的language sql，暂不修复
SELECT 'Geoserver3', "id",substr(encode(asBinary(force_2d("pt"),'XDR'),'base64'),1,16) as "pt" FROM "public"."wmstest" WHERE "pt" && GeomFromText('POLYGON ((-6.58216065979069 -0.7685569763184591, -6.58216065979069 0.911225433349509, -3.050569931030911 0.911225433349509, -3.050569931030911 -0.7685569763184591, -6.58216065979069 -0.7685569763184591))', 4326);
  ?column?  |  id  |        pt        
------------+------+------------------
 Geoserver3 | -500 | AAAAAAMAAAABAAAA
(1 row)

-- Run a Geoserver 2.0 NG KML query
SELECT 'Geoserver4', count(*) FROM "public"."wmstest" WHERE "pt" && GeomFromText('POLYGON ((-1.504017942347938 24.0332272532341, -1.504017942347938 25.99364254836741, 1.736833353559741 25.99364254836741, 1.736833353559741 24.0332272532341, -1.504017942347938 24.0332272532341))', 4326);
  ?column?  | count 
------------+-------
 Geoserver4 |     1
(1 row)

SELECT 'Geoserver5', "id",substr(encode(asBinary(force_2d("pt"),'XDR'),'base64'),1,16) as "pt" FROM "public"."wmstest" WHERE "pt" && GeomFromText('POLYGON ((-1.504017942347938 24.0332272532341, -1.504017942347938 25.99364254836741, 1.736833353559741 25.99364254836741, 1.736833353559741 24.0332272532341, -1.504017942347938 24.0332272532341))', 4326);
  ?column?  | id |        pt        
------------+----+------------------
 Geoserver5 | 25 | AAAAAAMAAAABAAAA
(1 row)

SELECT 'Geoserver6', "id",substr(encode(asBinary(force_2d("pt"),'XDR'),'base64'),1,16) as "pt" FROM "public"."wmstest" WHERE "pt" && GeomFromText('POLYGON ((-1.507182836191598 24.031312785172446, -1.507182836191598 25.995557016429064, 1.7399982474034008 25.995557016429064, 1.7399982474034008 24.031312785172446, -1.507182836191598 24.031312785172446))', 4326);
  ?column?  | id |        pt        
------------+----+------------------
 Geoserver6 | 25 | AAAAAAMAAAABAAAA
(1 row)

-- MapServer 5.4 tests
select 'MapServer1', attname from pg_attribute, pg_constraint, pg_class where pg_constraint.conrelid = pg_class.oid and pg_class.oid = pg_attribute.attrelid and pg_constraint.contype = 'p' and pg_constraint.conkey[1] = pg_attribute.attnum and pg_class.relname = 'wmstest' and pg_table_is_visible(pg_class.oid) and pg_constraint.conkey[2] is null;
  ?column?  | attname 
------------+---------
 MapServer1 | id
(1 row)

select 'MapServer2', "id",substr(encode(AsBinary(force_collection(force_2d("pt")),'NDR'),'base64'),1,16) as geom,"id" from wmstest where pt && GeomFromText('POLYGON((-98.5 32,-98.5 39,-91.5 39,-91.5 32,-98.5 32))',find_srid('','wmstest','pt')) order by "id";
  ?column?  |  id   |       geom       |  id   
------------+-------+------------------+-------
 MapServer2 | -9465 | AQcAAAABAAAAAQMA | -9465
 MapServer2 | -9460 | AQcAAAABAAAAAQMA | -9460
(2 rows)

-- MapServer 5.6 tests
select * from wmstest where false limit 0;
 id | pt 
----+----
(0 rows)

select 'MapServer3', attname from pg_attribute, pg_constraint, pg_class where pg_constraint.conrelid = pg_class.oid and pg_class.oid = pg_attribute.attrelid and pg_constraint.contype = 'p' and pg_constraint.conkey[1] = pg_attribute.attnum and pg_class.relname = 'wmstest' and pg_table_is_visible(pg_class.oid) and pg_constraint.conkey[2] is null;
  ?column?  | attname 
------------+---------
 MapServer3 | id
(1 row)

select 'MapServer4', "id",substr(encode(AsBinary(force_collection(force_2d("pt")),'NDR'),'hex'),1,16) as geom,"id" from wmstest where pt && GeomFromText('POLYGON((-98.5 32,-98.5 39,-91.5 39,-91.5 32,-98.5 32))',find_srid('','wmstest','pt')) order by "id";
  ?column?  |  id   |       geom       |  id   
------------+-------+------------------+-------
 MapServer4 | -9465 | 0107000000010000 | -9465
 MapServer4 | -9460 | 0107000000010000 | -9460
(2 rows)

-- Drop the data table
SELECT 'Removing the data table...';
          ?column?          
----------------------------
 Removing the data table...
(1 row)

DROP TABLE wmstest;
DROP TABLE
DELETE FROM geometry_columns WHERE f_table_name = 'wmstest' AND f_table_schema = 'public';
DELETE 0
SELECT 'Done.';
 ?column? 
----------
 Done.
(1 row)

-- test #1869 ST_AsBinary is not unique --
SELECT 1869 As ticket_id, ST_AsText(ST_AsBinary('POINT(1 2)'));
 ticket_id | st_astext  
-----------+------------
      1869 | POINT(1 2)
(1 row)

--\i uninstall_legacy.sql
\! gsql -q postgis_reg -f uninstall_legacy.sql > /dev/null 2>&1
