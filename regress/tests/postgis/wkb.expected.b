-- POINT
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POINT EMPTY'
::text as g ) as foo;
      g      |                   encode                   | st_orderingequals |                   encode                   
-------------+--------------------------------------------+-------------------+--------------------------------------------
 POINT EMPTY | 0101000000000000000000f87f000000000000f87f | t                 | 00000000017ff80000000000007ff8000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POINT Z EMPTY'
::text as g ) as foo;
       g       |                           encode                           | st_orderingequals |                           encode                           
---------------+------------------------------------------------------------+-------------------+------------------------------------------------------------
 POINT Z EMPTY | 01e9030000000000000000f87f000000000000f87f000000000000f87f | t                 | 00000003e97ff80000000000007ff80000000000007ff8000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POINT M EMPTY'
::text as g ) as foo;
       g       |                           encode                           | st_orderingequals |                           encode                           
---------------+------------------------------------------------------------+-------------------+------------------------------------------------------------
 POINT M EMPTY | 01d1070000000000000000f87f000000000000f87f000000000000f87f | t                 | 00000007d17ff80000000000007ff80000000000007ff8000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POINT ZM EMPTY'
::text as g ) as foo;
       g        |                                   encode                                   | st_orderingequals |                                   encode                                   
----------------+----------------------------------------------------------------------------+-------------------+----------------------------------------------------------------------------
 POINT ZM EMPTY | 01b90b0000000000000000f87f000000000000f87f000000000000f87f000000000000f87f | t                 | 0000000bb97ff80000000000007ff80000000000007ff80000000000007ff8000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POINT(0 0)'
::text as g ) as foo;
     g      |                   encode                   | st_orderingequals |                   encode                   
------------+--------------------------------------------+-------------------+--------------------------------------------
 POINT(0 0) | 010100000000000000000000000000000000000000 | t                 | 000000000100000000000000000000000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POINT Z (1 2 3)'
::text as g ) as foo;
        g        |                           encode                           | st_orderingequals |                           encode                           
-----------------+------------------------------------------------------------+-------------------+------------------------------------------------------------
 POINT Z (1 2 3) | 01e9030000000000000000f03f00000000000000400000000000000840 | t                 | 00000003e93ff000000000000040000000000000004008000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POINT M (1 2 3)'
::text as g ) as foo;
        g        |                           encode                           | st_orderingequals |                           encode                           
-----------------+------------------------------------------------------------+-------------------+------------------------------------------------------------
 POINT M (1 2 3) | 01d1070000000000000000f03f00000000000000400000000000000840 | t                 | 00000007d13ff000000000000040000000000000004008000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POINT ZM (1 2 3 4)'
::text as g ) as foo;
         g          |                                   encode                                   | st_orderingequals |                                   encode                                   
--------------------+----------------------------------------------------------------------------+-------------------+----------------------------------------------------------------------------
 POINT ZM (1 2 3 4) | 01b90b0000000000000000f03f000000000000004000000000000008400000000000001040 | t                 | 0000000bb93ff0000000000000400000000000000040080000000000004010000000000000
(1 row)

-- MULTIPOINT
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOINT EMPTY'
::text as g ) as foo;
        g         |       encode       | st_orderingequals |       encode       
------------------+--------------------+-------------------+--------------------
 MULTIPOINT EMPTY | 010400000000000000 | t                 | 000000000400000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOINT Z EMPTY'
::text as g ) as foo;
         g          |       encode       | st_orderingequals |       encode       
--------------------+--------------------+-------------------+--------------------
 MULTIPOINT Z EMPTY | 01ec03000000000000 | t                 | 00000003ec00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOINT M EMPTY'
::text as g ) as foo;
         g          |       encode       | st_orderingequals |       encode       
--------------------+--------------------+-------------------+--------------------
 MULTIPOINT M EMPTY | 01d407000000000000 | t                 | 00000007d400000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOINT ZM EMPTY'
::text as g ) as foo;
          g          |       encode       | st_orderingequals |       encode       
---------------------+--------------------+-------------------+--------------------
 MULTIPOINT ZM EMPTY | 01bc0b000000000000 | t                 | 0000000bbc00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOINT((0 0), (2 0))'
::text as g ) as foo;
            g             |                                                 encode                                                 | st_orderingequals |                                                 encode                                                 
--------------------------+--------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------
 MULTIPOINT((0 0), (2 0)) | 010400000002000000010100000000000000000000000000000000000000010100000000000000000000400000000000000000 | t                 | 000000000400000002000000000100000000000000000000000000000000000000000140000000000000000000000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOINT Z ((0 0 0), (2 0 1))'
::text as g ) as foo;
                g                |                                                                 encode                                                                 | st_orderingequals |                                                                 encode                                                                 
---------------------------------+----------------------------------------------------------------------------------------------------------------------------------------+-------------------+----------------------------------------------------------------------------------------------------------------------------------------
 MULTIPOINT Z ((0 0 0), (2 0 1)) | 01ec0300000200000001e903000000000000000000000000000000000000000000000000000001e903000000000000000000400000000000000000000000000000f03f | t                 | 00000003ec0000000200000003e900000000000000000000000000000000000000000000000000000003e9400000000000000000000000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOINT M ((0 0 2), (2 0 1))'
::text as g ) as foo;
                g                |                                                                 encode                                                                 | st_orderingequals |                                                                 encode                                                                 
---------------------------------+----------------------------------------------------------------------------------------------------------------------------------------+-------------------+----------------------------------------------------------------------------------------------------------------------------------------
 MULTIPOINT M ((0 0 2), (2 0 1)) | 01d40700000200000001d107000000000000000000000000000000000000000000000000004001d107000000000000000000400000000000000000000000000000f03f | t                 | 00000007d40000000200000007d100000000000000000000000000000000400000000000000000000007d1400000000000000000000000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOINT ZM ((0 1 2 3), (3 2 1 0))'
::text as g ) as foo;
                  g                   |                                                                                 encode                                                                                 | st_orderingequals |                                                                                 encode                                                                                 
--------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTIPOINT ZM ((0 1 2 3), (3 2 1 0)) | 01bc0b00000200000001b90b00000000000000000000000000000000f03f0000000000000040000000000000084001b90b000000000000000008400000000000000040000000000000f03f0000000000000000 | t                 | 0000000bbc000000020000000bb900000000000000003ff0000000000000400000000000000040080000000000000000000bb9400800000000000040000000000000003ff00000000000000000000000000000
(1 row)

-- LINESTRING
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'LINESTRING EMPTY'
::text as g ) as foo;
        g         |       encode       | st_orderingequals |       encode       
------------------+--------------------+-------------------+--------------------
 LINESTRING EMPTY | 010200000000000000 | t                 | 000000000200000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'LINESTRING Z EMPTY'
::text as g ) as foo;
         g          |       encode       | st_orderingequals |       encode       
--------------------+--------------------+-------------------+--------------------
 LINESTRING Z EMPTY | 01ea03000000000000 | t                 | 00000003ea00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'LINESTRING M EMPTY'
::text as g ) as foo;
         g          |       encode       | st_orderingequals |       encode       
--------------------+--------------------+-------------------+--------------------
 LINESTRING M EMPTY | 01d207000000000000 | t                 | 00000007d200000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'LINESTRING ZM EMPTY'
::text as g ) as foo;
          g          |       encode       | st_orderingequals |       encode       
---------------------+--------------------+-------------------+--------------------
 LINESTRING ZM EMPTY | 01ba0b000000000000 | t                 | 0000000bba00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'LINESTRING(0 0, 1 1)'
::text as g ) as foo;
          g           |                                       encode                                       | st_orderingequals |                                       encode                                       
----------------------+------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------
 LINESTRING(0 0, 1 1) | 01020000000200000000000000000000000000000000000000000000000000f03f000000000000f03f | t                 | 000000000200000002000000000000000000000000000000003ff00000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'LINESTRING Z (0 0 2, 1 1 3)'
::text as g ) as foo;
              g              |                                                       encode                                                       | st_orderingequals |                                                       encode                                                       
-----------------------------+--------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------
 LINESTRING Z (0 0 2, 1 1 3) | 01ea03000002000000000000000000000000000000000000000000000000000040000000000000f03f000000000000f03f0000000000000840 | t                 | 00000003ea000000020000000000000000000000000000000040000000000000003ff00000000000003ff00000000000004008000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'LINESTRING M (0 0 2, 1 1 3)'
::text as g ) as foo;
              g              |                                                       encode                                                       | st_orderingequals |                                                       encode                                                       
-----------------------------+--------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------
 LINESTRING M (0 0 2, 1 1 3) | 01d207000002000000000000000000000000000000000000000000000000000040000000000000f03f000000000000f03f0000000000000840 | t                 | 00000007d2000000020000000000000000000000000000000040000000000000003ff00000000000003ff00000000000004008000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'LINESTRING ZM (0 0 2 3, 1 1 4 5)'
::text as g ) as foo;
                g                 |                                                                       encode                                                                       | st_orderingequals |                                                                       encode                                                                       
----------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+----------------------------------------------------------------------------------------------------------------------------------------------------
 LINESTRING ZM (0 0 2 3, 1 1 4 5) | 01ba0b0000020000000000000000000000000000000000000000000000000000400000000000000840000000000000f03f000000000000f03f00000000000010400000000000001440 | t                 | 0000000bba0000000200000000000000000000000000000000400000000000000040080000000000003ff00000000000003ff000000000000040100000000000004014000000000000
(1 row)

-- MULTILINESTRING
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTILINESTRING EMPTY'
::text as g ) as foo;
           g           |       encode       | st_orderingequals |       encode       
-----------------------+--------------------+-------------------+--------------------
 MULTILINESTRING EMPTY | 010500000000000000 | t                 | 000000000500000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTILINESTRING Z EMPTY'
::text as g ) as foo;
            g            |       encode       | st_orderingequals |       encode       
-------------------------+--------------------+-------------------+--------------------
 MULTILINESTRING Z EMPTY | 01ed03000000000000 | t                 | 00000003ed00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTILINESTRING M EMPTY'
::text as g ) as foo;
            g            |       encode       | st_orderingequals |       encode       
-------------------------+--------------------+-------------------+--------------------
 MULTILINESTRING M EMPTY | 01d507000000000000 | t                 | 00000007d500000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTILINESTRING ZM EMPTY'
::text as g ) as foo;
            g             |       encode       | st_orderingequals |       encode       
--------------------------+--------------------+-------------------+--------------------
 MULTILINESTRING ZM EMPTY | 01bd0b000000000000 | t                 | 0000000bbd00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTILINESTRING((0 0, 2 0))'
::text as g ) as foo;
              g              |                                                encode                                                | st_orderingequals |                                                encode                                                
-----------------------------+------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------
 MULTILINESTRING((0 0, 2 0)) | 0105000000010000000102000000020000000000000000000000000000000000000000000000000000400000000000000000 | t                 | 0000000005000000010000000002000000020000000000000000000000000000000040000000000000000000000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTILINESTRING((0 0, 2 0), (1 1, 2 2))'
::text as g ) as foo;
                    g                    |                                                                                         encode                                                                                         | st_orderingequals |                                                                                         encode                                                                                         
-----------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTILINESTRING((0 0, 2 0), (1 1, 2 2)) | 0105000000020000000102000000020000000000000000000000000000000000000000000000000000400000000000000000010200000002000000000000000000f03f000000000000f03f00000000000000400000000000000040 | t                 | 00000000050000000200000000020000000200000000000000000000000000000000400000000000000000000000000000000000000002000000023ff00000000000003ff000000000000040000000000000004000000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTILINESTRING Z ((0 0 1, 2 0 2), (1 1 3, 2 2 4))'
::text as g ) as foo;
                         g                          |                                                                                                                         encode                                                                                                                         | st_orderingequals |                                                                                                                         encode                                                                                                                         
----------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTILINESTRING Z ((0 0 1, 2 0 2), (1 1 3, 2 2 4)) | 01ed0300000200000001ea0300000200000000000000000000000000000000000000000000000000f03f00000000000000400000000000000000000000000000004001ea03000002000000000000000000f03f000000000000f03f0000000000000840000000000000004000000000000000400000000000001040 | t                 | 00000003ed0000000200000003ea00000002000000000000000000000000000000003ff000000000000040000000000000000000000000000000400000000000000000000003ea000000023ff00000000000003ff00000000000004008000000000000400000000000000040000000000000004010000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTILINESTRING M ((0 0 1, 2 0 2), (1 1 3, 2 2 4))'
::text as g ) as foo;
                         g                          |                                                                                                                         encode                                                                                                                         | st_orderingequals |                                                                                                                         encode                                                                                                                         
----------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTILINESTRING M ((0 0 1, 2 0 2), (1 1 3, 2 2 4)) | 01d50700000200000001d20700000200000000000000000000000000000000000000000000000000f03f00000000000000400000000000000000000000000000004001d207000002000000000000000000f03f000000000000f03f0000000000000840000000000000004000000000000000400000000000001040 | t                 | 00000007d50000000200000007d200000002000000000000000000000000000000003ff000000000000040000000000000000000000000000000400000000000000000000007d2000000023ff00000000000003ff00000000000004008000000000000400000000000000040000000000000004010000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTILINESTRING ZM ((0 0 1 5, 2 0 2 4), (1 1 3 3, 2 2 4 2))'
::text as g ) as foo;
                              g                              |                                                                                                                                                         encode                                                                                                                                                         | st_orderingequals |                                                                                                                                                         encode                                                                                                                                                         
-------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTILINESTRING ZM ((0 0 1 5, 2 0 2 4), (1 1 3 3, 2 2 4 2)) | 01bd0b00000200000001ba0b00000200000000000000000000000000000000000000000000000000f03f0000000000001440000000000000004000000000000000000000000000000040000000000000104001ba0b000002000000000000000000f03f000000000000f03f000000000000084000000000000008400000000000000040000000000000004000000000000010400000000000000040 | t                 | 0000000bbd000000020000000bba00000002000000000000000000000000000000003ff0000000000000401400000000000040000000000000000000000000000000400000000000000040100000000000000000000bba000000023ff00000000000003ff0000000000000400800000000000040080000000000004000000000000000400000000000000040100000000000004000000000000000
(1 row)

-- POLYGON
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYGON EMPTY'
::text as g ) as foo;
       g       |       encode       | st_orderingequals |       encode       
---------------+--------------------+-------------------+--------------------
 POLYGON EMPTY | 010300000000000000 | t                 | 000000000300000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYGON Z EMPTY'
::text as g ) as foo;
        g        |       encode       | st_orderingequals |       encode       
-----------------+--------------------+-------------------+--------------------
 POLYGON Z EMPTY | 01eb03000000000000 | t                 | 00000003eb00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYGON M EMPTY'
::text as g ) as foo;
        g        |       encode       | st_orderingequals |       encode       
-----------------+--------------------+-------------------+--------------------
 POLYGON M EMPTY | 01d307000000000000 | t                 | 00000007d300000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYGON ZM EMPTY'
::text as g ) as foo;
        g         |       encode       | st_orderingequals |       encode       
------------------+--------------------+-------------------+--------------------
 POLYGON ZM EMPTY | 01bb0b000000000000 | t                 | 0000000bbb00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYGON((0 0,1 0,1 1,0 1,0 0))'
::text as g ) as foo;
               g                |                                                                                           encode                                                                                           | st_orderingequals |                                                                                           encode                                                                                           
--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 POLYGON((0 0,1 0,1 1,0 1,0 0)) | 0103000000010000000500000000000000000000000000000000000000000000000000f03f0000000000000000000000000000f03f000000000000f03f0000000000000000000000000000f03f00000000000000000000000000000000 | t                 | 00000000030000000100000005000000000000000000000000000000003ff000000000000000000000000000003ff00000000000003ff000000000000000000000000000003ff000000000000000000000000000000000000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYGON((0 0,10 0,10 10,0 10,0 0),(2 2,2 5,5 5,5 2,2 2))'
::text as g ) as foo;
                            g                             |                                                                                                                                                                               encode                                                                                                                                                                               | st_orderingequals |                                                                                                                                                                               encode                                                                                                                                                                               
----------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 POLYGON((0 0,10 0,10 10,0 10,0 0),(2 2,2 5,5 5,5 2,2 2)) | 010300000002000000050000000000000000000000000000000000000000000000000024400000000000000000000000000000244000000000000024400000000000000000000000000000244000000000000000000000000000000000050000000000000000000040000000000000004000000000000000400000000000001440000000000000144000000000000014400000000000001440000000000000004000000000000000400000000000000040 | t                 | 000000000300000002000000050000000000000000000000000000000040240000000000000000000000000000402400000000000040240000000000000000000000000000402400000000000000000000000000000000000000000000000000054000000000000000400000000000000040000000000000004014000000000000401400000000000040140000000000004014000000000000400000000000000040000000000000004000000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYGON Z ((0 0 1,10 0 2 ,10 10 2,0 10 2,0 0 1),(2 2 5 ,2 5 4,5 5 3,5 2 3,2 2 5))'
::text as g ) as foo;
                                         g                                         |                                                                                                                                                                                                                                                               encode                                                                                                                                                                                                                                                               | st_orderingequals |                                                                                                                                                                                                                                                               encode                                                                                                                                                                                                                                                               
-----------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 POLYGON Z ((0 0 1,10 0 2 ,10 10 2,0 10 2,0 0 1),(2 2 5 ,2 5 4,5 5 3,5 2 3,2 2 5)) | 01eb030000020000000500000000000000000000000000000000000000000000000000f03f00000000000024400000000000000000000000000000004000000000000024400000000000002440000000000000004000000000000000000000000000002440000000000000004000000000000000000000000000000000000000000000f03f05000000000000000000004000000000000000400000000000001440000000000000004000000000000014400000000000001040000000000000144000000000000014400000000000000840000000000000144000000000000000400000000000000840000000000000004000000000000000400000000000001440 | t                 | 00000003eb0000000200000005000000000000000000000000000000003ff0000000000000402400000000000000000000000000004000000000000000402400000000000040240000000000004000000000000000000000000000000040240000000000004000000000000000000000000000000000000000000000003ff000000000000000000005400000000000000040000000000000004014000000000000400000000000000040140000000000004010000000000000401400000000000040140000000000004008000000000000401400000000000040000000000000004008000000000000400000000000000040000000000000004014000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYGON M ((0 0 1,10 0 2 ,10 10 2,0 10 2,0 0 1),(2 2 5 ,2 5 4,5 5 3,5 2 3,2 2 5))'
::text as g ) as foo;
                                         g                                         |                                                                                                                                                                                                                                                               encode                                                                                                                                                                                                                                                               | st_orderingequals |                                                                                                                                                                                                                                                               encode                                                                                                                                                                                                                                                               
-----------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 POLYGON M ((0 0 1,10 0 2 ,10 10 2,0 10 2,0 0 1),(2 2 5 ,2 5 4,5 5 3,5 2 3,2 2 5)) | 01d3070000020000000500000000000000000000000000000000000000000000000000f03f00000000000024400000000000000000000000000000004000000000000024400000000000002440000000000000004000000000000000000000000000002440000000000000004000000000000000000000000000000000000000000000f03f05000000000000000000004000000000000000400000000000001440000000000000004000000000000014400000000000001040000000000000144000000000000014400000000000000840000000000000144000000000000000400000000000000840000000000000004000000000000000400000000000001440 | t                 | 00000007d30000000200000005000000000000000000000000000000003ff0000000000000402400000000000000000000000000004000000000000000402400000000000040240000000000004000000000000000000000000000000040240000000000004000000000000000000000000000000000000000000000003ff000000000000000000005400000000000000040000000000000004014000000000000400000000000000040140000000000004010000000000000401400000000000040140000000000004008000000000000401400000000000040000000000000004008000000000000400000000000000040000000000000004014000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYGON ZM ((0 0 1 -1,10 0 2 -2,10 10 2 -2,0 10 2 -4,0 0 1 -1),(2 2 5 0,2 5 4 1,5 5 3 2,5 2 3 1,2 2 5 0))'
::text as g ) as foo;
                                                     g                                                     |                                                                                                                                                                                                                                                                                                                                               encode                                                                                                                                                                                                                                                                                                                                               | st_orderingequals |                                                                                                                                                                                                                                                                                                                                               encode                                                                                                                                                                                                                                                                                                                                               
-----------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 POLYGON ZM ((0 0 1 -1,10 0 2 -2,10 10 2 -2,0 10 2 -4,0 0 1 -1),(2 2 5 0,2 5 4 1,5 5 3 2,5 2 3 1,2 2 5 0)) | 01bb0b0000020000000500000000000000000000000000000000000000000000000000f03f000000000000f0bf00000000000024400000000000000000000000000000004000000000000000c000000000000024400000000000002440000000000000004000000000000000c000000000000000000000000000002440000000000000004000000000000010c000000000000000000000000000000000000000000000f03f000000000000f0bf050000000000000000000040000000000000004000000000000014400000000000000000000000000000004000000000000014400000000000001040000000000000f03f0000000000001440000000000000144000000000000008400000000000000040000000000000144000000000000000400000000000000840000000000000f03f0000000000000040000000000000004000000000000014400000000000000000 | t                 | 0000000bbb0000000200000005000000000000000000000000000000003ff0000000000000bff0000000000000402400000000000000000000000000004000000000000000c000000000000000402400000000000040240000000000004000000000000000c000000000000000000000000000000040240000000000004000000000000000c010000000000000000000000000000000000000000000003ff0000000000000bff00000000000000000000540000000000000004000000000000000401400000000000000000000000000004000000000000000401400000000000040100000000000003ff000000000000040140000000000004014000000000000400800000000000040000000000000004014000000000000400000000000000040080000000000003ff00000000000004000000000000000400000000000000040140000000000000000000000000000
(1 row)

-- MULTIPOLYGON
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOLYGON EMPTY'
::text as g ) as foo;
         g          |       encode       | st_orderingequals |       encode       
--------------------+--------------------+-------------------+--------------------
 MULTIPOLYGON EMPTY | 010600000000000000 | t                 | 000000000600000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOLYGON Z EMPTY'
::text as g ) as foo;
          g           |       encode       | st_orderingequals |       encode       
----------------------+--------------------+-------------------+--------------------
 MULTIPOLYGON Z EMPTY | 01ee03000000000000 | t                 | 00000003ee00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOLYGON M EMPTY'
::text as g ) as foo;
          g           |       encode       | st_orderingequals |       encode       
----------------------+--------------------+-------------------+--------------------
 MULTIPOLYGON M EMPTY | 01d607000000000000 | t                 | 00000007d600000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOLYGON ZM EMPTY'
::text as g ) as foo;
           g           |       encode       | st_orderingequals |       encode       
-----------------------+--------------------+-------------------+--------------------
 MULTIPOLYGON ZM EMPTY | 01be0b000000000000 | t                 | 0000000bbe00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOLYGON(((0 0,10 0,10 10,0 10,0 0),(2 2,2 5,5 5,5 2,2 2)))'
::text as g ) as foo;
                                g                                |                                                                                                                                                                                        encode                                                                                                                                                                                        | st_orderingequals |                                                                                                                                                                                        encode                                                                                                                                                                                        
-----------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTIPOLYGON(((0 0,10 0,10 10,0 10,0 0),(2 2,2 5,5 5,5 2,2 2))) | 010600000001000000010300000002000000050000000000000000000000000000000000000000000000000024400000000000000000000000000000244000000000000024400000000000000000000000000000244000000000000000000000000000000000050000000000000000000040000000000000004000000000000000400000000000001440000000000000144000000000000014400000000000001440000000000000004000000000000000400000000000000040 | t                 | 000000000600000001000000000300000002000000050000000000000000000000000000000040240000000000000000000000000000402400000000000040240000000000000000000000000000402400000000000000000000000000000000000000000000000000054000000000000000400000000000000040000000000000004014000000000000401400000000000040140000000000004014000000000000400000000000000040000000000000004000000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOLYGON Z (((0 0 3,10 0 3,10 10 3,0 10 3,0 0 3),(2 2 3,2 5 3,5 5 3,5 2 3,2 2 3)))'
::text as g ) as foo;
                                           g                                            |                                                                                                                                                                                                                                                                        encode                                                                                                                                                                                                                                                                        | st_orderingequals |                                                                                                                                                                                                                                                                        encode                                                                                                                                                                                                                                                                        
----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTIPOLYGON Z (((0 0 3,10 0 3,10 10 3,0 10 3,0 0 3),(2 2 3,2 5 3,5 5 3,5 2 3,2 2 3))) | 01ee0300000100000001eb030000020000000500000000000000000000000000000000000000000000000000084000000000000024400000000000000000000000000000084000000000000024400000000000002440000000000000084000000000000000000000000000002440000000000000084000000000000000000000000000000000000000000000084005000000000000000000004000000000000000400000000000000840000000000000004000000000000014400000000000000840000000000000144000000000000014400000000000000840000000000000144000000000000000400000000000000840000000000000004000000000000000400000000000000840 | t                 | 00000003ee0000000100000003eb000000020000000500000000000000000000000000000000400800000000000040240000000000000000000000000000400800000000000040240000000000004024000000000000400800000000000000000000000000004024000000000000400800000000000000000000000000000000000000000000400800000000000000000005400000000000000040000000000000004008000000000000400000000000000040140000000000004008000000000000401400000000000040140000000000004008000000000000401400000000000040000000000000004008000000000000400000000000000040000000000000004008000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOLYGON M (((0 0 3,10 0 3,10 10 3,0 10 3,0 0 3),(2 2 3,2 5 3,5 5 3,5 2 3,2 2 3)))'
::text as g ) as foo;
                                           g                                            |                                                                                                                                                                                                                                                                        encode                                                                                                                                                                                                                                                                        | st_orderingequals |                                                                                                                                                                                                                                                                        encode                                                                                                                                                                                                                                                                        
----------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTIPOLYGON M (((0 0 3,10 0 3,10 10 3,0 10 3,0 0 3),(2 2 3,2 5 3,5 5 3,5 2 3,2 2 3))) | 01d60700000100000001d3070000020000000500000000000000000000000000000000000000000000000000084000000000000024400000000000000000000000000000084000000000000024400000000000002440000000000000084000000000000000000000000000002440000000000000084000000000000000000000000000000000000000000000084005000000000000000000004000000000000000400000000000000840000000000000004000000000000014400000000000000840000000000000144000000000000014400000000000000840000000000000144000000000000000400000000000000840000000000000004000000000000000400000000000000840 | t                 | 00000007d60000000100000007d3000000020000000500000000000000000000000000000000400800000000000040240000000000000000000000000000400800000000000040240000000000004024000000000000400800000000000000000000000000004024000000000000400800000000000000000000000000000000000000000000400800000000000000000005400000000000000040000000000000004008000000000000400000000000000040140000000000004008000000000000401400000000000040140000000000004008000000000000401400000000000040000000000000004008000000000000400000000000000040000000000000004008000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTIPOLYGON ZM (((0 0 3 2,10 0 3 2,10 10 3 2,0 10 3 2,0 0 3 2),(2 2 3 2,2 5 3 2,5 5 3 2,5 2 3 2,2 2 3 2)))'
::text as g ) as foo;
                                                      g                                                      |                                                                                                                                                                                                                                                                                                                                                        encode                                                                                                                                                                                                                                                                                                                                                        | st_orderingequals |                                                                                                                                                                                                                                                                                                                                                        encode                                                                                                                                                                                                                                                                                                                                                        
-------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTIPOLYGON ZM (((0 0 3 2,10 0 3 2,10 10 3 2,0 10 3 2,0 0 3 2),(2 2 3 2,2 5 3 2,5 5 3 2,5 2 3 2,2 2 3 2))) | 01be0b00000100000001bb0b00000200000005000000000000000000000000000000000000000000000000000840000000000000004000000000000024400000000000000000000000000000084000000000000000400000000000002440000000000000244000000000000008400000000000000040000000000000000000000000000024400000000000000840000000000000004000000000000000000000000000000000000000000000084000000000000000400500000000000000000000400000000000000040000000000000084000000000000000400000000000000040000000000000144000000000000008400000000000000040000000000000144000000000000014400000000000000840000000000000004000000000000014400000000000000040000000000000084000000000000000400000000000000040000000000000004000000000000008400000000000000040 | t                 | 0000000bbe000000010000000bbb0000000200000005000000000000000000000000000000004008000000000000400000000000000040240000000000000000000000000000400800000000000040000000000000004024000000000000402400000000000040080000000000004000000000000000000000000000000040240000000000004008000000000000400000000000000000000000000000000000000000000000400800000000000040000000000000000000000540000000000000004000000000000000400800000000000040000000000000004000000000000000401400000000000040080000000000004000000000000000401400000000000040140000000000004008000000000000400000000000000040140000000000004000000000000000400800000000000040000000000000004000000000000000400000000000000040080000000000004000000000000000
(1 row)

-- GEOMETRYCOLLECTION
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'GEOMETRYCOLLECTION EMPTY'
::text as g ) as foo;
            g             |       encode       | st_orderingequals |       encode       
--------------------------+--------------------+-------------------+--------------------
 GEOMETRYCOLLECTION EMPTY | 010700000000000000 | t                 | 000000000700000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'GEOMETRYCOLLECTION Z EMPTY'
::text as g ) as foo;
             g              |       encode       | st_orderingequals |       encode       
----------------------------+--------------------+-------------------+--------------------
 GEOMETRYCOLLECTION Z EMPTY | 01ef03000000000000 | t                 | 00000003ef00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'GEOMETRYCOLLECTION M EMPTY'
::text as g ) as foo;
             g              |       encode       | st_orderingequals |       encode       
----------------------------+--------------------+-------------------+--------------------
 GEOMETRYCOLLECTION M EMPTY | 01d707000000000000 | t                 | 00000007d700000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'GEOMETRYCOLLECTION ZM EMPTY'
::text as g ) as foo;
              g              |       encode       | st_orderingequals |       encode       
-----------------------------+--------------------+-------------------+--------------------
 GEOMETRYCOLLECTION ZM EMPTY | 01bf0b000000000000 | t                 | 0000000bbf00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'GEOMETRYCOLLECTION ZM (POINT ZM (0 0 0 0),LINESTRING ZM (0 0 0 0,1 1 1 1))'
::text as g ) as foo;
                                     g                                      |                                                                                                                     encode                                                                                                                     | st_orderingequals |                                                                                                                     encode                                                                                                                     
----------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 GEOMETRYCOLLECTION ZM (POINT ZM (0 0 0 0),LINESTRING ZM (0 0 0 0,1 1 1 1)) | 01bf0b00000200000001b90b0000000000000000000000000000000000000000000000000000000000000000000001ba0b0000020000000000000000000000000000000000000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f000000000000f03f | t                 | 0000000bbf000000020000000bb900000000000000000000000000000000000000000000000000000000000000000000000bba0000000200000000000000000000000000000000000000000000000000000000000000003ff00000000000003ff00000000000003ff00000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1))'
::text as g ) as foo;
                                 g                                 |                                                                                             encode                                                                                             | st_orderingequals |                                                                                             encode                                                                                             
-------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1)) | 01d70700000200000001d107000000000000000000000000000000000000000000000000000001d207000002000000000000000000000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f | t                 | 00000007d70000000200000007d100000000000000000000000000000000000000000000000000000007d2000000020000000000000000000000000000000000000000000000003ff00000000000003ff00000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1),GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1)))'
::text as g ) as foo;
                                                                  g                                                                  |                                                                                                                                                                                            encode                                                                                                                                                                                            | st_orderingequals |                                                                                                                                                                                            encode                                                                                                                                                                                            
-------------------------------------------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1),GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1))) | 01d70700000300000001d107000000000000000000000000000000000000000000000000000001d207000002000000000000000000000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f01d70700000200000001d107000000000000000000000000000000000000000000000000000001d207000002000000000000000000000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f | t                 | 00000007d70000000300000007d100000000000000000000000000000000000000000000000000000007d2000000020000000000000000000000000000000000000000000000003ff00000000000003ff00000000000003ff000000000000000000007d70000000200000007d100000000000000000000000000000000000000000000000000000007d2000000020000000000000000000000000000000000000000000000003ff00000000000003ff00000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1),POINT M EMPTY,GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1)))'
::text as g ) as foo;
                                                                         g                                                                         |                                                                                                                                                                                                                         encode                                                                                                                                                                                                                         | st_orderingequals |                                                                                                                                                                                                                         encode                                                                                                                                                                                                                         
---------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1),POINT M EMPTY,GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1))) | 01d70700000400000001d107000000000000000000000000000000000000000000000000000001d207000002000000000000000000000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f01d1070000000000000000f87f000000000000f87f000000000000f87f01d70700000200000001d107000000000000000000000000000000000000000000000000000001d207000002000000000000000000000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f | t                 | 00000007d70000000400000007d100000000000000000000000000000000000000000000000000000007d2000000020000000000000000000000000000000000000000000000003ff00000000000003ff00000000000003ff000000000000000000007d17ff80000000000007ff80000000000007ff800000000000000000007d70000000200000007d100000000000000000000000000000000000000000000000000000007d2000000020000000000000000000000000000000000000000000000003ff00000000000003ff00000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1),GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1),POINT M EMPTY,GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1))),POINT M EMPTY,GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1)))'
::text as g ) as foo;
                                                                                                                                                  g                                                                                                                                                  |                                                                                                                                                                                                                                                                                                                                                                                                                                                    encode                                                                                                                                                                                                                                                                                                                                                                                                                                                    | st_orderingequals |                                                                                                                                                                                                                                                                                                                                                                                                                                                    encode                                                                                                                                                                                                                                                                                                                                                                                                                                                    
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1),GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1),POINT M EMPTY,GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1))),POINT M EMPTY,GEOMETRYCOLLECTION M (POINT M (0 0 0),LINESTRING M (0 0 0,1 1 1))) | 01d70700000500000001d107000000000000000000000000000000000000000000000000000001d207000002000000000000000000000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f01d70700000400000001d107000000000000000000000000000000000000000000000000000001d207000002000000000000000000000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f01d1070000000000000000f87f000000000000f87f000000000000f87f01d70700000200000001d107000000000000000000000000000000000000000000000000000001d207000002000000000000000000000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f01d1070000000000000000f87f000000000000f87f000000000000f87f01d70700000200000001d107000000000000000000000000000000000000000000000000000001d207000002000000000000000000000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f | t                 | 00000007d70000000500000007d100000000000000000000000000000000000000000000000000000007d2000000020000000000000000000000000000000000000000000000003ff00000000000003ff00000000000003ff000000000000000000007d70000000400000007d100000000000000000000000000000000000000000000000000000007d2000000020000000000000000000000000000000000000000000000003ff00000000000003ff00000000000003ff000000000000000000007d17ff80000000000007ff80000000000007ff800000000000000000007d70000000200000007d100000000000000000000000000000000000000000000000000000007d2000000020000000000000000000000000000000000000000000000003ff00000000000003ff00000000000003ff000000000000000000007d17ff80000000000007ff80000000000007ff800000000000000000007d70000000200000007d100000000000000000000000000000000000000000000000000000007d2000000020000000000000000000000000000000000000000000000003ff00000000000003ff00000000000003ff0000000000000
(1 row)

-- CIRCULARSTRING
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CIRCULARSTRING EMPTY'
::text as g ) as foo;
          g           |       encode       | st_orderingequals |       encode       
----------------------+--------------------+-------------------+--------------------
 CIRCULARSTRING EMPTY | 010800000000000000 | t                 | 000000000800000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CIRCULARSTRING Z EMPTY'
::text as g ) as foo;
           g            |       encode       | st_orderingequals |       encode       
------------------------+--------------------+-------------------+--------------------
 CIRCULARSTRING Z EMPTY | 01f003000000000000 | t                 | 00000003f000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CIRCULARSTRING M EMPTY'
::text as g ) as foo;
           g            |       encode       | st_orderingequals |       encode       
------------------------+--------------------+-------------------+--------------------
 CIRCULARSTRING M EMPTY | 01d807000000000000 | t                 | 00000007d800000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CIRCULARSTRING ZM EMPTY'
::text as g ) as foo;
            g            |       encode       | st_orderingequals |       encode       
-------------------------+--------------------+-------------------+--------------------
 CIRCULARSTRING ZM EMPTY | 01c00b000000000000 | t                 | 0000000bc000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CIRCULARSTRING (0 0,1 1, 2 0)'
::text as g ) as foo;
               g               |                                                       encode                                                       | st_orderingequals |                                                       encode                                                       
-------------------------------+--------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------
 CIRCULARSTRING (0 0,1 1, 2 0) | 01080000000300000000000000000000000000000000000000000000000000f03f000000000000f03f00000000000000400000000000000000 | t                 | 000000000800000003000000000000000000000000000000003ff00000000000003ff000000000000040000000000000000000000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CIRCULARSTRING M (0 0 1,1 1 1, 2 0 1)'
::text as g ) as foo;
                   g                   |                                                                               encode                                                                               | st_orderingequals |                                                                               encode                                                                               
---------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------
 CIRCULARSTRING M (0 0 1,1 1 1, 2 0 1) | 01d80700000300000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f000000000000f03f00000000000000400000000000000000000000000000f03f | t                 | 00000007d800000003000000000000000000000000000000003ff00000000000003ff00000000000003ff00000000000003ff0000000000000400000000000000000000000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CIRCULARSTRING ZM (0 0 1 2,1 1 1 2, 2 0 1 2)'
::text as g ) as foo;
                      g                       |                                                                                                       encode                                                                                                       | st_orderingequals |                                                                                                       encode                                                                                                       
----------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 CIRCULARSTRING ZM (0 0 1 2,1 1 1 2, 2 0 1 2) | 01c00b00000300000000000000000000000000000000000000000000000000f03f0000000000000040000000000000f03f000000000000f03f000000000000f03f000000000000004000000000000000400000000000000000000000000000f03f0000000000000040 | t                 | 0000000bc000000003000000000000000000000000000000003ff000000000000040000000000000003ff00000000000003ff00000000000003ff00000000000004000000000000000400000000000000000000000000000003ff00000000000004000000000000000
(1 row)

-- COMPOUNDCURVE
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'COMPOUNDCURVE EMPTY'
::text as g ) as foo;
          g          |       encode       | st_orderingequals |       encode       
---------------------+--------------------+-------------------+--------------------
 COMPOUNDCURVE EMPTY | 010900000000000000 | t                 | 000000000900000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'COMPOUNDCURVE Z EMPTY'
::text as g ) as foo;
           g           |       encode       | st_orderingequals |       encode       
-----------------------+--------------------+-------------------+--------------------
 COMPOUNDCURVE Z EMPTY | 01f103000000000000 | t                 | 00000003f100000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'COMPOUNDCURVE M EMPTY'
::text as g ) as foo;
           g           |       encode       | st_orderingequals |       encode       
-----------------------+--------------------+-------------------+--------------------
 COMPOUNDCURVE M EMPTY | 01d907000000000000 | t                 | 00000007d900000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'COMPOUNDCURVE ZM EMPTY'
::text as g ) as foo;
           g            |       encode       | st_orderingequals |       encode       
------------------------+--------------------+-------------------+--------------------
 COMPOUNDCURVE ZM EMPTY | 01c10b000000000000 | t                 | 0000000bc100000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'COMPOUNDCURVE (CIRCULARSTRING (0 0,1 1,2 0),LINESTRING(2 0,4 1))'
::text as g ) as foo;
                                g                                 |                                                                                                         encode                                                                                                         | st_orderingequals |                                                                                                         encode                                                                                                         
------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 COMPOUNDCURVE (CIRCULARSTRING (0 0,1 1,2 0),LINESTRING(2 0,4 1)) | 01090000000200000001080000000300000000000000000000000000000000000000000000000000f03f000000000000f03f00000000000000400000000000000000010200000002000000000000000000004000000000000000000000000000001040000000000000f03f | t                 | 000000000900000002000000000800000003000000000000000000000000000000003ff00000000000003ff0000000000000400000000000000000000000000000000000000002000000024000000000000000000000000000000040100000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'COMPOUNDCURVE Z (CIRCULARSTRING Z (0 0 1,1 1 1,2 0 1),LINESTRING Z (2 0 0,4 1 1))'
::text as g ) as foo;
                                         g                                         |                                                                                                                                                 encode                                                                                                                                                 | st_orderingequals |                                                                                                                                                 encode                                                                                                                                                 
-----------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 COMPOUNDCURVE Z (CIRCULARSTRING Z (0 0 1,1 1 1,2 0 1),LINESTRING Z (2 0 0,4 1 1)) | 01f10300000200000001f00300000300000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f000000000000f03f00000000000000400000000000000000000000000000f03f01ea030000020000000000000000000040000000000000000000000000000000000000000000001040000000000000f03f000000000000f03f | t                 | 00000003f10000000200000003f000000003000000000000000000000000000000003ff00000000000003ff00000000000003ff00000000000003ff0000000000000400000000000000000000000000000003ff000000000000000000003ea0000000240000000000000000000000000000000000000000000000040100000000000003ff00000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'COMPOUNDCURVE M (CIRCULARSTRING M (0 0 1,1 1 1,2 0 1),LINESTRING M (2 0 0,4 1 1))'
::text as g ) as foo;
                                         g                                         |                                                                                                                                                 encode                                                                                                                                                 | st_orderingequals |                                                                                                                                                 encode                                                                                                                                                 
-----------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 COMPOUNDCURVE M (CIRCULARSTRING M (0 0 1,1 1 1,2 0 1),LINESTRING M (2 0 0,4 1 1)) | 01d90700000200000001d80700000300000000000000000000000000000000000000000000000000f03f000000000000f03f000000000000f03f000000000000f03f00000000000000400000000000000000000000000000f03f01d2070000020000000000000000000040000000000000000000000000000000000000000000001040000000000000f03f000000000000f03f | t                 | 00000007d90000000200000007d800000003000000000000000000000000000000003ff00000000000003ff00000000000003ff00000000000003ff0000000000000400000000000000000000000000000003ff000000000000000000007d20000000240000000000000000000000000000000000000000000000040100000000000003ff00000000000003ff0000000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'COMPOUNDCURVE ZM (CIRCULARSTRING ZM (0 0 1 2,1 1 1 2,2 0 1 2),LINESTRING ZM (2 0 0 0,4 1 1 1))'
::text as g ) as foo;
                                               g                                                |                                                                                                                                                                                         encode                                                                                                                                                                                         | st_orderingequals |                                                                                                                                                                                         encode                                                                                                                                                                                         
------------------------------------------------------------------------------------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 COMPOUNDCURVE ZM (CIRCULARSTRING ZM (0 0 1 2,1 1 1 2,2 0 1 2),LINESTRING ZM (2 0 0 0,4 1 1 1)) | 01c10b00000200000001c00b00000300000000000000000000000000000000000000000000000000f03f0000000000000040000000000000f03f000000000000f03f000000000000f03f000000000000004000000000000000400000000000000000000000000000f03f000000000000004001ba0b00000200000000000000000000400000000000000000000000000000000000000000000000000000000000001040000000000000f03f000000000000f03f000000000000f03f | t                 | 0000000bc1000000020000000bc000000003000000000000000000000000000000003ff000000000000040000000000000003ff00000000000003ff00000000000003ff00000000000004000000000000000400000000000000000000000000000003ff000000000000040000000000000000000000bba00000002400000000000000000000000000000000000000000000000000000000000000040100000000000003ff00000000000003ff00000000000003ff0000000000000
(1 row)

-- CURVEPOLYGON
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CURVEPOLYGON EMPTY'
::text as g ) as foo;
         g          |       encode       | st_orderingequals |       encode       
--------------------+--------------------+-------------------+--------------------
 CURVEPOLYGON EMPTY | 010a00000000000000 | t                 | 000000000a00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CURVEPOLYGON Z EMPTY'
::text as g ) as foo;
          g           |       encode       | st_orderingequals |       encode       
----------------------+--------------------+-------------------+--------------------
 CURVEPOLYGON Z EMPTY | 01f203000000000000 | t                 | 00000003f200000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CURVEPOLYGON M EMPTY'
::text as g ) as foo;
          g           |       encode       | st_orderingequals |       encode       
----------------------+--------------------+-------------------+--------------------
 CURVEPOLYGON M EMPTY | 01da07000000000000 | t                 | 00000007da00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CURVEPOLYGON ZM EMPTY'
::text as g ) as foo;
           g           |       encode       | st_orderingequals |       encode       
-----------------------+--------------------+-------------------+--------------------
 CURVEPOLYGON ZM EMPTY | 01c20b000000000000 | t                 | 0000000bc200000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'CURVEPOLYGON ZM (COMPOUNDCURVE ZM (CIRCULARSTRING ZM (0 0 1 2,1 1 1 2,2 0 1 2),LINESTRING(2 0 1 2,1 -1 1 1,0 0 1 2)))'
::text as g ) as foo;
                                                           g                                                           |                                                                                                                                                                                                                                  encode                                                                                                                                                                                                                                  | st_orderingequals |                                                                                                                                                                                                                                  encode                                                                                                                                                                                                                                  
-----------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 CURVEPOLYGON ZM (COMPOUNDCURVE ZM (CIRCULARSTRING ZM (0 0 1 2,1 1 1 2,2 0 1 2),LINESTRING(2 0 1 2,1 -1 1 1,0 0 1 2))) | 01c20b00000100000001c10b00000200000001c00b00000300000000000000000000000000000000000000000000000000f03f0000000000000040000000000000f03f000000000000f03f000000000000f03f000000000000004000000000000000400000000000000000000000000000f03f000000000000004001ba0b00000300000000000000000000400000000000000000000000000000f03f0000000000000040000000000000f03f000000000000f0bf000000000000f03f000000000000f03f00000000000000000000000000000000000000000000f03f0000000000000040 | t                 | 0000000bc2000000010000000bc1000000020000000bc000000003000000000000000000000000000000003ff000000000000040000000000000003ff00000000000003ff00000000000003ff00000000000004000000000000000400000000000000000000000000000003ff000000000000040000000000000000000000bba00000003400000000000000000000000000000003ff000000000000040000000000000003ff0000000000000bff00000000000003ff00000000000003ff0000000000000000000000000000000000000000000003ff00000000000004000000000000000
(1 row)

-- MULTICURVE
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTICURVE EMPTY'
::text as g ) as foo;
        g         |       encode       | st_orderingequals |       encode       
------------------+--------------------+-------------------+--------------------
 MULTICURVE EMPTY | 010b00000000000000 | t                 | 000000000b00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTICURVE Z EMPTY'
::text as g ) as foo;
         g          |       encode       | st_orderingequals |       encode       
--------------------+--------------------+-------------------+--------------------
 MULTICURVE Z EMPTY | 01f303000000000000 | t                 | 00000003f300000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTICURVE M EMPTY'
::text as g ) as foo;
         g          |       encode       | st_orderingequals |       encode       
--------------------+--------------------+-------------------+--------------------
 MULTICURVE M EMPTY | 01db07000000000000 | t                 | 00000007db00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTICURVE ZM EMPTY'
::text as g ) as foo;
          g          |       encode       | st_orderingequals |       encode       
---------------------+--------------------+-------------------+--------------------
 MULTICURVE ZM EMPTY | 01c30b000000000000 | t                 | 0000000bc300000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTICURVE ZM (COMPOUNDCURVE ZM (CIRCULARSTRING ZM (0 0 1 2,1 1 1 2,2 0 1 2),LINESTRING(2 0 1 2,1 -1 1 1,0 0 1 2)))'
::text as g ) as foo;
                                                          g                                                          |                                                                                                                                                                                                                                  encode                                                                                                                                                                                                                                  | st_orderingequals |                                                                                                                                                                                                                                  encode                                                                                                                                                                                                                                  
---------------------------------------------------------------------------------------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTICURVE ZM (COMPOUNDCURVE ZM (CIRCULARSTRING ZM (0 0 1 2,1 1 1 2,2 0 1 2),LINESTRING(2 0 1 2,1 -1 1 1,0 0 1 2))) | 01c30b00000100000001c10b00000200000001c00b00000300000000000000000000000000000000000000000000000000f03f0000000000000040000000000000f03f000000000000f03f000000000000f03f000000000000004000000000000000400000000000000000000000000000f03f000000000000004001ba0b00000300000000000000000000400000000000000000000000000000f03f0000000000000040000000000000f03f000000000000f0bf000000000000f03f000000000000f03f00000000000000000000000000000000000000000000f03f0000000000000040 | t                 | 0000000bc3000000010000000bc1000000020000000bc000000003000000000000000000000000000000003ff000000000000040000000000000003ff00000000000003ff00000000000003ff00000000000004000000000000000400000000000000000000000000000003ff000000000000040000000000000000000000bba00000003400000000000000000000000000000003ff000000000000040000000000000003ff0000000000000bff00000000000003ff00000000000003ff0000000000000000000000000000000000000000000003ff00000000000004000000000000000
(1 row)

-- MULTISURFACE
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTISURFACE EMPTY'
::text as g ) as foo;
         g          |       encode       | st_orderingequals |       encode       
--------------------+--------------------+-------------------+--------------------
 MULTISURFACE EMPTY | 010c00000000000000 | t                 | 000000000c00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTISURFACE Z EMPTY'
::text as g ) as foo;
          g           |       encode       | st_orderingequals |       encode       
----------------------+--------------------+-------------------+--------------------
 MULTISURFACE Z EMPTY | 01f403000000000000 | t                 | 00000003f400000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTISURFACE M EMPTY'
::text as g ) as foo;
          g           |       encode       | st_orderingequals |       encode       
----------------------+--------------------+-------------------+--------------------
 MULTISURFACE M EMPTY | 01dc07000000000000 | t                 | 00000007dc00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTISURFACE ZM EMPTY'
::text as g ) as foo;
           g           |       encode       | st_orderingequals |       encode       
-----------------------+--------------------+-------------------+--------------------
 MULTISURFACE ZM EMPTY | 01c40b000000000000 | t                 | 0000000bc400000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'MULTISURFACE ZM (CURVEPOLYGON ZM (COMPOUNDCURVE ZM (CIRCULARSTRING ZM (0 0 1 2,1 1 1 2,2 0 1 2),LINESTRING(2 0 1 2,1 -1 1 1,0 0 1 2))),POLYGON((10 10 10 10,10 12 10 10,12 12 10 10,12 10 10 10,10 10 10 10)))'
::text as g ) as foo;
                                                                                                       g                                                                                                        |                                                                                                                                                                                                                                                                                                                                                                                                                        encode                                                                                                                                                                                                                                                                                                                                                                                                                        | st_orderingequals |                                                                                                                                                                                                                                                                                                                                                                                                                        encode                                                                                                                                                                                                                                                                                                                                                                                                                        
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 MULTISURFACE ZM (CURVEPOLYGON ZM (COMPOUNDCURVE ZM (CIRCULARSTRING ZM (0 0 1 2,1 1 1 2,2 0 1 2),LINESTRING(2 0 1 2,1 -1 1 1,0 0 1 2))),POLYGON((10 10 10 10,10 12 10 10,12 12 10 10,12 10 10 10,10 10 10 10))) | 01c40b00000200000001c20b00000100000001c10b00000200000001c00b00000300000000000000000000000000000000000000000000000000f03f0000000000000040000000000000f03f000000000000f03f000000000000f03f000000000000004000000000000000400000000000000000000000000000f03f000000000000004001ba0b00000300000000000000000000400000000000000000000000000000f03f0000000000000040000000000000f03f000000000000f0bf000000000000f03f000000000000f03f00000000000000000000000000000000000000000000f03f000000000000004001bb0b0000010000000500000000000000000024400000000000002440000000000000244000000000000024400000000000002440000000000000284000000000000024400000000000002440000000000000284000000000000028400000000000002440000000000000244000000000000028400000000000002440000000000000244000000000000024400000000000002440000000000000244000000000000024400000000000002440 | t                 | 0000000bc4000000020000000bc2000000010000000bc1000000020000000bc000000003000000000000000000000000000000003ff000000000000040000000000000003ff00000000000003ff00000000000003ff00000000000004000000000000000400000000000000000000000000000003ff000000000000040000000000000000000000bba00000003400000000000000000000000000000003ff000000000000040000000000000003ff0000000000000bff00000000000003ff00000000000003ff0000000000000000000000000000000000000000000003ff000000000000040000000000000000000000bbb000000010000000540240000000000004024000000000000402400000000000040240000000000004024000000000000402800000000000040240000000000004024000000000000402800000000000040280000000000004024000000000000402400000000000040280000000000004024000000000000402400000000000040240000000000004024000000000000402400000000000040240000000000004024000000000000
(1 row)

-- POLYHEDRALSURFACE
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYHEDRALSURFACE EMPTY'
::text as g ) as foo;
            g            |       encode       | st_orderingequals |       encode       
-------------------------+--------------------+-------------------+--------------------
 POLYHEDRALSURFACE EMPTY | 010f00000000000000 | t                 | 000000000f00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYHEDRALSURFACE Z EMPTY'
::text as g ) as foo;
             g             |       encode       | st_orderingequals |       encode       
---------------------------+--------------------+-------------------+--------------------
 POLYHEDRALSURFACE Z EMPTY | 01f703000000000000 | t                 | 00000003f700000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYHEDRALSURFACE M EMPTY'
::text as g ) as foo;
             g             |       encode       | st_orderingequals |       encode       
---------------------------+--------------------+-------------------+--------------------
 POLYHEDRALSURFACE M EMPTY | 01df07000000000000 | t                 | 00000007df00000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'POLYHEDRALSURFACE ZM EMPTY'
::text as g ) as foo;
             g              |       encode       | st_orderingequals |       encode       
----------------------------+--------------------+-------------------+--------------------
 POLYHEDRALSURFACE ZM EMPTY | 01c70b000000000000 | t                 | 0000000bc700000000
(1 row)

-- TRIANGLE
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'TRIANGLE EMPTY'
::text as g ) as foo;
       g        |       encode       | st_orderingequals |       encode       
----------------+--------------------+-------------------+--------------------
 TRIANGLE EMPTY | 011100000000000000 | t                 | 000000001100000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'TRIANGLE Z EMPTY'
::text as g ) as foo;
        g         |       encode       | st_orderingequals |       encode       
------------------+--------------------+-------------------+--------------------
 TRIANGLE Z EMPTY | 01f903000000000000 | t                 | 00000003f900000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'TRIANGLE M EMPTY'
::text as g ) as foo;
        g         |       encode       | st_orderingequals |       encode       
------------------+--------------------+-------------------+--------------------
 TRIANGLE M EMPTY | 01e107000000000000 | t                 | 00000007e100000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'TRIANGLE ZM EMPTY'
::text as g ) as foo;
         g         |       encode       | st_orderingequals |       encode       
-------------------+--------------------+-------------------+--------------------
 TRIANGLE ZM EMPTY | 01c90b000000000000 | t                 | 0000000bc900000000
(1 row)

-- TIN
select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'TIN EMPTY'
::text as g ) as foo;
     g     |       encode       | st_orderingequals |       encode       
-----------+--------------------+-------------------+--------------------
 TIN EMPTY | 011000000000000000 | t                 | 000000001000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'TIN Z EMPTY'
::text as g ) as foo;
      g      |       encode       | st_orderingequals |       encode       
-------------+--------------------+-------------------+--------------------
 TIN Z EMPTY | 01f803000000000000 | t                 | 00000003f800000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'TIN M EMPTY'
::text as g ) as foo;
      g      |       encode       | st_orderingequals |       encode       
-------------+--------------------+-------------------+--------------------
 TIN M EMPTY | 01e007000000000000 | t                 | 00000007e000000000
(1 row)

select g, encode(st_asbinary(g::geometry, 'ndr'), 'hex'),
          st_orderingequals(g::geometry, ST_GeomFromWKB(ST_AsBinary(g::geometry))),
          encode(st_asbinary(g::geometry, 'xdr'), 'hex') FROM ( SELECT
 'TIN ZM EMPTY'
::text as g ) as foo;
      g       |       encode       | st_orderingequals |       encode       
--------------+--------------------+-------------------+--------------------
 TIN ZM EMPTY | 01c80b000000000000 | t                 | 0000000bc800000000
(1 row)

-- Crashes (#4767, #4768, #4771, #4772)
SELECT st_mpointfromwkb('\x'::BYTEA);
ERROR:  Invalid endian flag value encountered.
SELECT geometry('\x'::bytea);
ERROR:  Invalid endian flag value encountered.
SELECT st_geomcollfromwkb('\x'::bytea);
ERROR:  Invalid endian flag value encountered.
SELECT st_geomfromewkb('\x'::bytea);
ERROR:  Invalid endian flag value encountered.
SELECT st_wkbtosql('\x');
ERROR:  Invalid endian flag value encountered.
SELECT st_mpolyfromwkb('\x'::BYTEA);
ERROR:  Invalid endian flag value encountered.
SELECT st_mlinefromwkb('\x'::bytea);
ERROR:  Invalid endian flag value encountered.
SELECT st_mlinefromwkb('\x'::bytea,1);
ERROR:  Invalid endian flag value encountered.
