SELECT 't1', path, ST_AsText(geom)
FROM (
  SELECT (ST_Dump(g.geom)).*
  FROM
    (SELECT
       'POINT (0 9)'::geometry AS geom
    ) AS g
  ) j;
 ?column? | path | st_astext  
----------+------+------------
 t1       | {}   | POINT(0 9)
(1 row)

SELECT 't2', path, ST_AsText(geom)
FROM (
  SELECT (ST_Dump(g.geom)).*
  FROM
    (SELECT
       'LINESTRING (
                0 0,
                0 9,
                9 9,
                9 0,
                0 0
            )'::geometry AS geom
    ) AS g
  ) j;
 ?column? | path |            st_astext            
----------+------+---------------------------------
 t2       | {}   | LINESTRING(0 0,0 9,9 9,9 0,0 0)
(1 row)

SELECT 't3', path, ST_AsText(geom)
FROM (
  SELECT (ST_Dump(g.geom)).*
  FROM
    (SELECT
       'POLYGON ((
                0 0,
                0 9,
                9 9,
                9 0,
                0 0
            ))'::geometry AS geom
    ) AS g
  ) j;
 ?column? | path |           st_astext            
----------+------+--------------------------------
 t3       | {}   | POLYGON((0 0,0 9,9 9,9 0,0 0))
(1 row)

SELECT 't4', path, ST_AsText(geom)
FROM (
  SELECT (ST_Dump(g.geom)).*
  FROM
    (SELECT
       'TRIANGLE ((
                0 0,
                0 9,
                9 0,
                0 0
            ))'::geometry AS geom
    ) AS g
  ) j;
 ?column? | path |          st_astext          
----------+------+-----------------------------
 t4       | {}   | TRIANGLE((0 0,0 9,9 0,0 0))
(1 row)

SELECT 't5', path, ST_AsText(geom)
FROM (
  SELECT (ST_Dump(g.geom)).*
  FROM
    (SELECT
       'POLYGON ((
                0 0,
                0 9,
                9 9,
                9 0,
                0 0
            ), (
                1 1,
                1 3,
                3 2,
                1 1
            ), (
                7 6,
                6 8,
                8 8,
                7 6
            ))'::geometry AS geom
    ) AS g
  ) j;
 ?column? | path |                             st_astext                              
----------+------+--------------------------------------------------------------------
 t5       | {}   | POLYGON((0 0,0 9,9 9,9 0,0 0),(1 1,1 3,3 2,1 1),(7 6,6 8,8 8,7 6))
(1 row)

SELECT 't6', path, ST_AsText(geom)
FROM (
  SELECT (ST_Dump(g.geom)).*
  FROM
    (SELECT
       'MULTIPOLYGON (((
                0 0,
                0 3,
                4 3,
                4 0,
                0 0
            )), ((
                2 4,
                1 6,
                4 5,
                2 4
            ), (
                7 6,
                6 8,
                8 8,
                7 6
            )))'::geometry AS geom
    ) AS g
  ) j;
 ?column? | path |                  st_astext                   
----------+------+----------------------------------------------
 t6       | {1}  | POLYGON((0 0,0 3,4 3,4 0,0 0))
 t6       | {2}  | POLYGON((2 4,1 6,4 5,2 4),(7 6,6 8,8 8,7 6))
(2 rows)

SELECT 't7', path, ST_AsEWKT(geom)
FROM (
  SELECT (ST_Dump(g.geom)).*
   FROM
     (SELECT
       'POLYHEDRALSURFACE (((
                0 0 0,
                0 0 1,
                0 1 1,
                0 1 0,
                0 0 0
            )), ((
                0 0 0,
                0 1 0,
                1 1 0,
                1 0 0,
                0 0 0
            ))
            )'::geometry AS geom
   ) AS g
  ) j;
 ?column? | path |                st_asewkt                 
----------+------+------------------------------------------
 t7       | {1}  | POLYGON((0 0 0,0 0 1,0 1 1,0 1 0,0 0 0))
 t7       | {2}  | POLYGON((0 0 0,0 1 0,1 1 0,1 0 0,0 0 0))
(2 rows)

SELECT 't8', path, ST_AsEWKT(geom)
FROM (
  SELECT (ST_Dump(g.geom)).*
   FROM
     (SELECT
       'TIN (((
                0 0 0,
                0 0 1,
                0 1 0,
                0 0 0
            )), ((
                0 0 0,
                0 1 0,
                1 1 0,
                0 0 0
            ))
            )'::geometry AS geom
   ) AS g
  ) j;
 ?column? | path |              st_asewkt              
----------+------+-------------------------------------
 t8       | {1}  | TRIANGLE((0 0 0,0 0 1,0 1 0,0 0 0))
 t8       | {2}  | TRIANGLE((0 0 0,0 1 0,1 1 0,0 0 0))
(2 rows)

SELECT 't9', path, ST_AsText(geom)
FROM (
  SELECT (ST_Dump(g.geom)).*
  FROM
    (SELECT
       'GEOMETRYCOLLECTION(
          POINT(99 98),
          LINESTRING(1 1, 3 3),
          POLYGON((0 0, 0 1, 1 1, 0 0)),
          POLYGON((0 0, 0 9, 9 9, 9 0, 0 0), (5 5, 5 6, 6 6, 5 5)),
          MULTIPOLYGON(((0 0, 0 9, 9 9, 9 0, 0 0), (5 5, 5 6, 6 6, 5 5)))
        )'::geometry AS geom
    ) AS g
  ) j;
 ?column? | path  |                    st_astext                     
----------+-------+--------------------------------------------------
 t9       | {1}   | POINT(99 98)
 t9       | {2}   | LINESTRING(1 1,3 3)
 t9       | {3}   | POLYGON((0 0,0 1,1 1,0 0))
 t9       | {4}   | POLYGON((0 0,0 9,9 9,9 0,0 0),(5 5,5 6,6 6,5 5))
 t9       | {5,1} | POLYGON((0 0,0 9,9 9,9 0,0 0),(5 5,5 6,6 6,5 5))
(5 rows)

SELECT 't10', count(*)
FROM ST_Dump('
GEOMETRYCOLLECTION EMPTY
');
 ?column? | count 
----------+-------
 t10      |     0
(1 row)

SELECT 't11', count(*)
FROM ST_Dump('
GEOMETRYCOLLECTION (
	GEOMETRYCOLLECTION EMPTY,
	POINT EMPTY,
	LINESTRING EMPTY,
	POLYGON EMPTY,
	MULTIPOINT EMPTY,
	MULTILINESTRING EMPTY,
	MULTIPOLYGON EMPTY,
	GEOMETRYCOLLECTION (
		GEOMETRYCOLLECTION EMPTY
	)
)
');
 ?column? | count 
----------+-------
 t11      |     0
(1 row)

-- Check that it works without the extension schema being available
BEGIN;
BEGIN
SET search_path TO pg_catalog;
SET
WITH data AS
(
    SELECT 'POLYGON((0 0, 0 1, 1 1, 1 0, 0 0))':: :schema geometry as geom
)
SELECT	't12',
	:schema ST_AsText(( :schema ST_Dump(geom)).geom),
	:schema ST_AsText(( :schema ST_DumpRings(geom)).geom) FROM data;
 ?column? |           st_astext            |           st_astext            
----------+--------------------------------+--------------------------------
 t12      | POLYGON((0 0,0 1,1 1,1 0,0 0)) | POLYGON((0 0,0 1,1 1,1 0,0 0))
(1 row)

COMMIT;
COMMIT
