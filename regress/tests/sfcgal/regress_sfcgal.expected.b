---
--- Regression tests for PostGIS SFCGAL backend
---
-- We only care about testing PostGIS prototype here
-- Behaviour is already handled by SFCGAL own tests
SELECT 'postgis_sfcgal_version', count(*) FROM (SELECT postgis_sfcgal_version()) AS f;
        ?column?        | count 
------------------------+-------
 postgis_sfcgal_version |     1
(1 row)

SELECT 'ST_Tesselate', ST_AsText(ST_Tesselate('GEOMETRYCOLLECTION(POINT(4 4),POLYGON((0 0,1 0,1 1,0 1,0 0)))'));
   ?column?   |                                  st_astext                                  
--------------+-----------------------------------------------------------------------------
 ST_Tesselate | GEOMETRYCOLLECTION(POINT(4 4),TIN(((0 1,1 0,1 1,0 1)),((0 1,0 0,1 0,0 1))))
(1 row)

SELECT 'ST_3DArea', ST_3DArea('POLYGON((0 0 0,1 0 0,1 1 0,0 1 0,0 0 0))');
 ?column?  | st_3darea 
-----------+-----------
 ST_3DArea |         1
(1 row)

SELECT 'ST_Extrude_point', ST_AsText(ST_Extrude('POINT(0 0)', 1, 0, 0));
     ?column?     |         st_astext          
------------------+----------------------------
 ST_Extrude_point | LINESTRING Z (0 0 0,1 0 0)
(1 row)

SELECT 'ST_Extrude_line', ST_AsText(ST_Extrude(ST_Extrude('POINT(0 0)', 1, 0, 0), 0, 1, 0));
    ?column?     |                        st_astext                        
-----------------+---------------------------------------------------------
 ST_Extrude_line | POLYHEDRALSURFACE Z (((0 0 0,1 0 0,1 1 0,0 1 0,0 0 0)))
(1 row)

-- In the first SFCGAL versions, the extruded face was wrongly oriented
-- we change the extrusion result to match the original
SELECT 'ST_Extrude_surface',
CASE WHEN postgis_sfcgal_version() = '1.0'
THEN
    ST_AsText(ST_Extrude(ST_Extrude(ST_Extrude('POINT(0 0)', 1, 0, 0), 0, 1, 0), 0, 0, 1))
ELSE
    regexp_replace(
    regexp_replace(
    ST_AsText(ST_Extrude(ST_Extrude(ST_Extrude('POINT(0 0)', 1, 0, 0), 0, 1, 0), 0, 0, 1)) ,
    '\(\(0 1 0,1 1 0,1 0 0,0 1 0\)\)', '((1 1 0,1 0 0,0 1 0,1 1 0))'),
    '\(\(0 1 0,1 0 0,0 0 0,0 1 0\)\)', '((1 0 0,0 0 0,0 1 0,1 0 0))')
END;
      ?column?      |                                                                                                                                    regexp_replace                                                                                                                                     
--------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 ST_Extrude_surface | POLYHEDRALSURFACE Z (((((1 1 0,1 0 0,0 1 0,1 1 0)))),((0 1 1,1 0 1,1 1 1,0 1 1)),((((1 0 0,0 0 0,0 1 0,1 0 0)))),((0 1 1,0 0 1,1 0 1,0 1 1)),((1 0 0,1 1 0,1 1 1,1 0 1,1 0 0)),((1 1 0,0 1 0,0 1 1,1 1 1,1 1 0)),((0 1 0,0 0 0,0 0 1,0 1 1,0 1 0)),((0 0 0,1 0 0,1 0 1,0 0 1,0 0 0)))
(1 row)

SELECT 'ST_ForceLHR', ST_AsText(ST_ForceLHR('POLYGON((0 0,0 1,1 1,1 0,0 0))'));
  ?column?   |           st_astext            
-------------+--------------------------------
 ST_ForceLHR | POLYGON((0 0,1 0,1 1,0 1,0 0))
(1 row)

SELECT 'ST_Orientation_1', ST_Orientation(ST_ForceLHR('POLYGON((0 0,0 1,1 1,1 0,0 0))'));
     ?column?     | st_orientation 
------------------+----------------
 ST_Orientation_1 |             -1
(1 row)

SELECT 'ST_Orientation_2', ST_Orientation(ST_ForceRHR('POLYGON((0 0,0 1,1 1,1 0,0 0))'));
     ?column?     | st_orientation 
------------------+----------------
 ST_Orientation_2 |              1
(1 row)

SELECT 'ST_MinkowskiSum', ST_AsText(ST_MinkowskiSum('LINESTRING(0 0,4 0)','POLYGON((0 0,1 0,1 1,0 1,0 0))'));
    ?column?     |                   st_astext                   
-----------------+-----------------------------------------------
 ST_MinkowskiSum | MULTIPOLYGON(((0 0,1 0,5 0,5 1,4 1,0 1,0 0)))
(1 row)

SELECT 'ST_StraightSkeleton', ST_AsText(ST_StraightSkeleton('POLYGON((1 1,2 1,2 2,1 2,1 1))'));
      ?column?       |                                st_astext                                 
---------------------+--------------------------------------------------------------------------
 ST_StraightSkeleton | MULTILINESTRING((1 1,1.5 1.5),(2 1,1.5 1.5),(2 2,1.5 1.5),(1 2,1.5 1.5))
(1 row)

SELECT 'ST_ConstrainedDelaunayTriangles', ST_AsText(ST_ConstrainedDelaunayTriangles('GEOMETRYCOLLECTION(POINT(0 0), POLYGON((2 2, 2 -2, 4 0, 2 2)))'));
            ?column?             |                   st_astext                    
---------------------------------+------------------------------------------------
 ST_ConstrainedDelaunayTriangles | TIN(((4 0,2 2,2 -2,4 0)),((2 2,0 0,2 -2,2 2)))
(1 row)

SELECT 'postgis_sfcgal_noop', ST_NPoints(postgis_sfcgal_noop(ST_Buffer('POINT(0 0)', 5)));
      ?column?       | st_npoints 
---------------------+------------
 postgis_sfcgal_noop |         33
(1 row)

