# **********************************************************************
# *
# * PostGIS - Spatial Types for PostgreSQL
# * http://postgis.net
# *
# * Copyright (C) 2020 Sandro Santilli <strk@kbt.io>
# *
# * This is free software; you can redistribute and/or modify it under
# * the terms of the GNU General Public Licence. See the COPYING file.
# *
# **********************************************************************
override RUNTESTFLAGS := $(RUNTESTFLAGS) --sfcgal --extension

TESTS += \
		$(topsrcdir)/sfcgal/regress/regress_sfcgal \
		$(topsrcdir)/sfcgal/regress/approximatemedialaxis.sql

