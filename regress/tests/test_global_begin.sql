-- 2215之后mysql兼容模式的pi值改变，与其它兼容模式不同，所以将测试用例中的pi都改为stable_pi统一结果
create function stable_pi() returns float8 as
    'begin return 3.14159265358979323846::float8; end'
language plpgsql stable;
