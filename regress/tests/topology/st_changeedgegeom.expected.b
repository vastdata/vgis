set client_min_messages to ERROR;
SET
\i ./load_sql/load_topology.sql
--
-- From examples in chapter 1.12.1 of
-- "Spatial Topology and Network Data Models" (Oracle manual)
--
-- Modified to use postgis-based topology model.
-- Loads the whole topology represented in Figure 1-1 of the
-- manual
--
--ORA---------------------------------------------------------------------
--ORA---- Main steps for using the topology data model with a topology
--ORA---- built from edge, node, and face data
--ORA---------------------------------------------------------------------
--ORA---- 1. Create a topology.
--ORA---- 2. Load (normally bulk-load) topology data
--ORA----    (node, edge, and face tables).
BEGIN;
BEGIN
-- 1. Create the topology.
--
-- NOTE:
--  Returns topology id... which depend on how many
--  topologies where created in the regress database
--  so we just check it is a number greater than 0
--
SELECT NULL FROM topology.CreateTopology('city_data', -1);
 ?column? 
----------
 
(1 row)

update pg_depend set deptype = 'z' where deptype = 'a';
UPDATE 414
-- 2. Load topology data (node, edge, and face tables).
-- Use INSERT statements here instead of a bulk-load utility.
-- 2A. Insert data into <topology_name>.FACE table.
INSERT INTO city_data.face(face_id) VALUES(1); -- F1
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(2); -- F2
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(3); -- F3
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(4); -- F4
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(5); -- F5
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(6); -- F6
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(7); -- F7
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(8); -- F8
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(9); -- F9
INSERT 0 1
-- UPDATE Face id sequence
SELECT NULL FROM setval('city_data.face_face_id_seq', 9);
 ?column? 
----------
 
(1 row)

-- 2B. Insert data into <topology_name>.NODE table.
-- N1
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(1, 'SRID=-1;POINT(8 30)', NULL);
INSERT 0 1
-- N2
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(2, 'SRID=-1;POINT(25 30)', NULL);
INSERT 0 1
-- N3
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(3, 'SRID=-1;POINT(25 35)', NULL);
INSERT 0 1
-- N4
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(4, 'SRID=-1;POINT(20 37)', 2);
INSERT 0 1
-- N5
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(5, 'SRID=-1;POINT(36 38)', NULL);
INSERT 0 1
-- N6
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(6, 'SRID=-1;POINT(57 33)', NULL);
INSERT 0 1
-- N7
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(7, 'SRID=-1;POINT(41 40)', NULL);
INSERT 0 1
-- N8
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(8, 'SRID=-1;POINT(9 6)', NULL);
INSERT 0 1
-- N9
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(9, 'SRID=-1;POINT(21 6)', NULL);
INSERT 0 1
-- N10
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(10, 'SRID=-1;POINT(35 6)', NULL);
INSERT 0 1
-- N11
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(11, 'SRID=-1;POINT(47 6)', NULL);
INSERT 0 1
-- N12
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(12, 'SRID=-1;POINT(47 14)', NULL);
INSERT 0 1
-- N13
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(13, 'SRID=-1;POINT(35 14)', NULL);
INSERT 0 1
-- N14
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(14, 'SRID=-1;POINT(21 14)', NULL);
INSERT 0 1
-- N15
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(15, 'SRID=-1;POINT(9 14)', NULL);
INSERT 0 1
-- N16
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(16, 'SRID=-1;POINT(9 22)', NULL);
INSERT 0 1
-- N17
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(17, 'SRID=-1;POINT(21 22)', NULL);
INSERT 0 1
-- N18
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(18, 'SRID=-1;POINT(35 22)', NULL);
INSERT 0 1
-- N19
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(19, 'SRID=-1;POINT(47 22)', NULL);
INSERT 0 1
-- N20
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(20, 'SRID=-1;POINT(4 31)', NULL);
INSERT 0 1
-- N21
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(21, 'SRID=-1;POINT(9 35)', NULL);
INSERT 0 1
-- N22
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(22, 'SRID=-1;POINT(13 35)', NULL);
INSERT 0 1
-- UPDATE Node id sequence
SELECT NULL FROM setval('city_data.node_node_id_seq', 22);
 ?column? 
----------
 
(1 row)

-- 2C. Insert data into <topology_name>.EDGE table.
-- E1
INSERT INTO city_data.edge VALUES(1, 1, 1, 1, -1, 1, 0,
  'SRID=-1;LINESTRING(8 30, 16 30, 16 38, 3 38, 3 30, 8 30)');
INSERT 0 1
-- E2
INSERT INTO city_data.edge VALUES(2, 2, 2, 3, -2, 2, 0,
  'SRID=-1;LINESTRING(25 30, 31 30, 31 40, 17 40, 17 30, 25 30)');
INSERT 0 1
-- E3
INSERT INTO city_data.edge VALUES(3, 2, 3, -3, 2, 2, 2,
  'SRID=-1;LINESTRING(25 30, 25 35)');
INSERT 0 1
-- E4
INSERT INTO city_data.edge VALUES(4, 5, 6, -5, 4, 0, 0,
    'SRID=-1;LINESTRING(36 38, 38 35, 41 34, 42 33, 45 32, 47 28, 50 28, 52 32, 57 33)');
INSERT 0 1
-- E5
INSERT INTO city_data.edge VALUES(5, 7, 6, -4, 5, 0, 0,
    'SRID=-1;LINESTRING(41 40, 45 40, 47 42, 62 41, 61 38, 59 39, 57 36, 57 33)');
INSERT 0 1
-- E6
INSERT INTO city_data.edge VALUES(6, 16, 17, 7, -21, 0, 3,
    'SRID=-1;LINESTRING(9 22, 21 22)');
INSERT 0 1
-- E7
INSERT INTO city_data.edge VALUES(7, 17, 18, 8, -19, 0, 4,
    'SRID=-1;LINESTRING(21 22, 35 22)');
INSERT 0 1
-- E8
INSERT INTO city_data.edge VALUES(8, 18, 19, -15, -17, 0, 5,
    'SRID=-1;LINESTRING(35 22, 47 22)');
INSERT 0 1
-- E9
INSERT INTO city_data.edge VALUES(9, 15, 14, 19, -22, 3, 6,
    'SRID=-1;LINESTRING(9 14, 21 14)');
INSERT 0 1
-- E10
INSERT INTO city_data.edge VALUES(10, 13, 14, -20, 17, 7, 4,
    'SRID=-1;LINESTRING(35 14, 21 14)');
INSERT 0 1
-- E11
INSERT INTO city_data.edge VALUES(11, 13, 12, 15, -18, 5, 8,
    'SRID=-1;LINESTRING(35 14, 47 14)');
INSERT 0 1
-- E12
INSERT INTO city_data.edge VALUES(12, 8, 9, 20, 22, 6, 0,
    'SRID=-1;LINESTRING(9 6, 21 6)');
INSERT 0 1
-- E13
INSERT INTO city_data.edge VALUES(13, 9, 10, 18, -12, 7, 0,
    'SRID=-1;LINESTRING(21 6, 35 6)');
INSERT 0 1
-- E14
INSERT INTO city_data.edge VALUES(14, 10, 11, 16, -13, 8, 0,
    'SRID=-1;LINESTRING(35 6, 47 6)');
INSERT 0 1
-- E15
INSERT INTO city_data.edge VALUES(15, 12, 19, -8, -16, 5, 0,
    'SRID=-1;LINESTRING(47 14, 47 22)');
INSERT 0 1
-- E16
INSERT INTO city_data.edge VALUES(16, 11, 12, -11, -14, 8, 0,
    'SRID=-1;LINESTRING(47 6, 47 14)');
INSERT 0 1
-- E17
INSERT INTO city_data.edge VALUES(17, 13, 18, -7, 11, 4, 5,
    'SRID=-1;LINESTRING(35 14, 35 22)');
INSERT 0 1
-- E18
INSERT INTO city_data.edge VALUES(18, 10, 13, 10, 14, 7, 8,
    'SRID=-1;LINESTRING(35 6, 35 14)');
INSERT 0 1
-- E19
INSERT INTO city_data.edge VALUES(19, 14, 17, -6, -10, 3, 4,
    'SRID=-1;LINESTRING(21 14, 21 22)');
INSERT 0 1
-- E20
INSERT INTO city_data.edge VALUES(20, 9, 14, -9, 13, 6, 7,
    'SRID=-1;LINESTRING(21 6, 21 14)');
INSERT 0 1
-- E21
INSERT INTO city_data.edge VALUES(21, 15, 16, 6, 9, 0, 3,
    'SRID=-1;LINESTRING(9 14, 9 22)');
INSERT 0 1
-- E22
INSERT INTO city_data.edge VALUES(22, 8, 15, 21, 12, 0, 6,
    'SRID=-1;LINESTRING(9 6, 9 14)');
INSERT 0 1
-- E25
INSERT INTO city_data.edge VALUES(25, 21, 22, -25, 25, 1, 1,
  'SRID=-1;LINESTRING(9 35, 13 35)');
INSERT 0 1
-- E26
INSERT INTO city_data.edge VALUES(26, 20, 20, 26, -26, 9, 1,
  'SRID=-1;LINESTRING(4 31, 7 31, 7 34, 4 34, 4 31)');
INSERT 0 1
-- UPDATE Edge id sequence
SELECT NULL FROM setval('city_data.edge_data_edge_id_seq', 26);
 ?column? 
----------
 
(1 row)

-- Set face minimum bounding rectangle
UPDATE city_data.face set mbr = ST_SetSRID( ( select st_extent(geom) from city_data.edge where left_face = face_id or right_face = face_id ), -1 ) where face_id != 0;
UPDATE 9
update pg_depend set deptype = 'a' where deptype = 'z';
UPDATE 414
END;
COMMIT
-- good one
SELECT 'T1', topology.ST_ChangeEdgeGeom('city_data', 25,
 'LINESTRING(9 35, 11 33, 13 35)');
 ?column? | st_changeedgegeom 
----------+-------------------
 T1       | Edge 25 changed
(1 row)

-- start/end points mismatch
SELECT topology.ST_ChangeEdgeGeom('city_data', 25,
 'LINESTRING(10 35, 13 35)');
ERROR:  SQL/MM Spatial exception - start node not geometry start point.
SELECT topology.ST_ChangeEdgeGeom('city_data', 25,
 'LINESTRING(9 35, 13 36)');
ERROR:  SQL/MM Spatial exception - end node not geometry end point.
-- Node crossing
SELECT topology.ST_ChangeEdgeGeom('city_data', 3,
  'LINESTRING(25 30, 20 36, 20 38, 25 35)');
ERROR:  SQL/MM Spatial exception - geometry crosses a node
-- Non-simple edge
SELECT topology.ST_ChangeEdgeGeom('city_data', 1,
  'LINESTRING(8 30, 9 30, 8 30)');
ERROR:  SQL/MM Spatial exception - curve not simple
-- Dimensionally collapsed edge (#1774)
SELECT topology.ST_ChangeEdgeGeom('city_data', 1,
  'LINESTRING(8 30, 8 30, 8 30)');
ERROR:  Invalid edge (no two distinct vertices exist)
-- Non-existent edge (#979)
SELECT topology.ST_ChangeEdgeGeom('city_data', 666,
  'LINESTRING(25 30, 20 36, 20 38, 25 35)');
ERROR:  SQL/MM Spatial exception - non-existent edge 666
-- Test edge crossing
SELECT topology.ST_ChangeEdgeGeom('city_data', 25,
 'LINESTRING(9 35, 11 40, 13 35)');
ERROR:  SQL/MM Spatial exception - geometry crosses edge 1
-- Test change in presence of edges sharing node (#1428)
SELECT 'T2', topology.ST_ChangeEdgeGeom('city_data', 5,
 'LINESTRING(41 40, 57 33)');
 ?column? | st_changeedgegeom 
----------+-------------------
 T2       | Edge 5 changed
(1 row)

-- Change to edge crossing old self
SELECT 'T3', topology.ST_ChangeEdgeGeom('city_data', 5,
 'LINESTRING(41 40, 49 40, 49 34, 57 33)');
 ?column? | st_changeedgegeom 
----------+-------------------
 T3       | Edge 5 changed
(1 row)

-- Change a closed edge (counterclockwise)
SELECT 'T4', topology.ST_ChangeEdgeGeom('city_data', 26,
 'LINESTRING(4 31, 7 31, 4 33, 4 31)');
 ?column? | st_changeedgegeom 
----------+-------------------
 T4       | Edge 26 changed
(1 row)

-- Check face update
SELECT 'T4F', ST_Equals(f.mbr, ST_Envelope(e.geom))
 FROM city_data.face f, city_data.edge e
 WHERE e.edge_id = 26 AND f.face_id = e.left_face;
 ?column? | st_equals 
----------+-----------
 T4F      | t
(1 row)

-- Collisions on edge motion path is forbidden:
-- get to include a whole isolated edge
SELECT topology.ST_ChangeEdgeGeom('city_data', 26,
 'LINESTRING(4 31, 7 31, 15 34, 12 37.5, 4 34, 4 31)');
ERROR:  Edge motion collision at POINT(9 35)
-- This movement doesn't collide:
SELECT 'T5', topology.ST_ChangeEdgeGeom('city_data', 3,
 'LINESTRING(25 30, 18 35, 18 39, 23 39, 23 36, 20 38, 19 37, 20 35, 25 35)');
 ?column? | st_changeedgegeom 
----------+-------------------
 T5       | Edge 3 changed
(1 row)

-- This movement doesn't collide either:
SELECT 'T6', topology.ST_ChangeEdgeGeom('city_data', 3,
 'LINESTRING(25 30, 22 38, 25 35)');
 ?column? | st_changeedgegeom 
----------+-------------------
 T6       | Edge 3 changed
(1 row)

-- This movement gets to include an isolated node:
SELECT topology.ST_ChangeEdgeGeom('city_data', 3,
 'LINESTRING(25 30, 18 35, 18 39, 23 39, 23 36, 20 35, 25 35)');
ERROR:  Edge motion collision at POINT(20 37)
-- This movement is legit (counterclockwise closed edge)
SELECT 'T7', topology.ST_ChangeEdgeGeom('city_data', 2,
 'LINESTRING(25 30, 28 39, 16 39, 25 30)');
 ?column? | st_changeedgegeom 
----------+-------------------
 T7       | Edge 2 changed
(1 row)

-- Check face update
SELECT 'T7F', ST_Equals(f.mbr, ST_Envelope(e.geom))
 FROM city_data.face f, city_data.edge e
 WHERE e.edge_id = 2 AND f.face_id = e.left_face;
 ?column? | st_equals 
----------+-----------
 T7F      | t
(1 row)

-- This movement gets to exclude an isolated node:
SELECT topology.ST_ChangeEdgeGeom('city_data', 2,
 'LINESTRING(25 30, 28 39, 20 39, 25 30)');
ERROR:  Edge motion collision at POINT(20 37)
-- This movement should be fine
SELECT 'T7.1', topology.ST_ChangeEdgeGeom('city_data', 2,
'LINESTRING(25 30, 28 39, 17 39, 25 30)');
 ?column? | st_changeedgegeom 
----------+-------------------
 T7.1     | Edge 2 changed
(1 row)

-- Check face update
SELECT 'T7F.1',
  ST_Equals(f.mbr, ST_Envelope(ST_GetFaceGeometry('city_data', f.face_id::int4)))
  FROM city_data.face f, city_data.edge e
  WHERE e.edge_id = 2 AND f.face_id = e.left_face;
 ?column? | st_equals 
----------+-----------
 T7F.1    | t
(1 row)

-- Test changing winding direction of closed edge
SELECT topology.ST_ChangeEdgeGeom('city_data', 26,
 ST_Reverse('LINESTRING(4 31, 7 31, 4 34, 4 31)'));
ERROR:  Edge twist at node POINT(4 31)
-- Maintain winding of closed edge (counterclockwise)
SELECT 'T8', topology.ST_ChangeEdgeGeom('city_data', 26,
 'LINESTRING(4 31, 4 30.4, 5 30.4, 4 31)');
 ?column? | st_changeedgegeom 
----------+-------------------
 T8       | Edge 26 changed
(1 row)

-- Check face update
SELECT 'T8F',
  ST_Equals(f.mbr, ST_Envelope(ST_GetFaceGeometry('city_data', f.face_id::int4)))
  FROM city_data.face f, city_data.edge e
  WHERE e.edge_id = 26 AND f.face_id = e.left_face;
 ?column? | st_equals 
----------+-----------
 T8F      | t
(1 row)

-- test changing winding of non-closed edge ring
SELECT topology.ST_ChangeEdgeGeom('city_data', 13,
 'LINESTRING(21 6, 21 2, 6 2, 6 25, 50 25, 50 2, 35 2, 35 6)');
ERROR:  Edge motion collision at POINT(9 6)
-- test moving closed edge into another face
SELECT 'T9', ST_AddEdgeModFace('city_data', 20, 20,
 'LINESTRING(4 31, 7 31, 4 34, 4 31)');
 ?column? | st_addedgemodface 
----------+-------------------
 T9       |                27
(1 row)

SELECT ST_ChangeEdgeGeom('city_data', 26, -- should fail!
 'LINESTRING(4 31,5 31.5,4.6 32,4 31)');
ERROR:  Edge changed disposition around start node 20
-- test moving non-closed edge into another face
SELECT 'T10', ST_AddEdgeModFace('city_data', 17, 18,
 'LINESTRING(21 22, 28 27, 35 22)');
 ?column? | st_addedgemodface 
----------+-------------------
 T10      |                28
(1 row)

SELECT ST_ChangeEdgeGeom('city_data', 28, -- should fail!
 'LINESTRING(21 22, 28 18, 35 22)');
ERROR:  Edge changed disposition around start node 17
-- test enlarging a face MBR by moving an edge
SELECT 'T11', ST_ChangeEdgeGeom('city_data', 16,
 'LINESTRING(47 6, 51 10, 47 14)');
 ?column? | st_changeedgegeom 
----------+-------------------
 T11      | Edge 16 changed
(1 row)

-- Check face update
SELECT 'T11F',
  ST_Equals(f.mbr, ST_Envelope(ST_GetFaceGeometry('city_data', f.face_id::int4)))
  FROM city_data.face f, city_data.edge e
  WHERE e.edge_id = 16 AND f.face_id = e.left_face;
 ?column? | st_equals 
----------+-----------
 T11F     | t
(1 row)

-- See http://trac.osgeo.org/postgis/ticket/1775
SELECT 'T12.1', ST_AddIsoNode('city_data', 8, 'POINT(49 10)');
 ?column? | st_addisonode 
----------+---------------
 T12.1    |            23
(1 row)

SELECT 'T12', ST_ChangeEdgeGeom('city_data', 16, 'LINESTRING(47 6, 47 14)');
ERROR:  Edge motion collision at POINT(49 10)
-- See http://trac.osgeo.org/postgis/ticket/2176
SELECT 'T13.1', TopoGeo_AddLineString('city_data', '01020000001D000000E42CEC69873FF2BF9E98F56228E347400EDB16653648F2BF4985B18520E34740E92B4833164DF2BF3A1E335019E34740A94D9CDCEF50F2BF33F9669B1BE347407DAEB6627F59F2BF2CF180B229E34740758E01D9EB5DF2BFD0D556EC2FE34740533F6F2A5261F2BFD717096D39E34740F4893C49BA66F2BFC8073D9B55E34740B8239C16BC68F2BF33A7CB6262E34740AA2B9FE57970F2BF4165FCFB8CE347406DC5FEB27B72F2BFBA4E232D95E34740978BF84ECC7AF2BF24EEB1F4A1E34740E527D53E1D8FF2BF8F8D40BCAEE3474036CD3B4ED191F2BF649291B3B0E34740841266DAFE95F2BF1DE6CB0BB0E34740E3361AC05BA0F2BFB2632310AFE347405C5A0D897BACF2BF72F90FE9B7E3474031D3F6AFACB4F2BF4F232D95B7E347402B137EA99FB7F2BFD656EC2FBBE347402D431CEBE2B6F2BF551344DD07E4474011E4A08499B6F2BF15E3FC4D28E447406519E25817B7F2BF63EE5A423EE447409DD7D825AAB7F2BFE3FC4D2844E447405969520ABABDF2BF2384471B47E44740A31EA2D11DC4F2BFB1F9B83654E447400473F4F8BDCDF2BFEA5BE67459E447405070B1A206D3F2BFF19D98F562E4474062670A9DD7D8F2BF0E4FAF9465E447407FF6234564D8F2BFF1BA7EC16EE44740' );
 ?column? | topogeo_addlinestring 
----------+-----------------------
 T13.1    |                    29
(1 row)

SELECT 'T13.2', ST_ChangeEdgeGeom('city_data', 29, '010200000008000000E42CEC69873FF2BF9E98F56228E34740E92B4833164DF2BF3B1E335019E34740768E01D9EB5DF2BFD0D556EC2FE347406EC5FEB27B72F2BFBA4E232D95E34740988BF84ECC7AF2BF25EEB1F4A1E347402C137EA99FB7F2BFD656EC2FBBE347409DD7D825AAB7F2BFE4FC4D2844E447407FF6234564D8F2BFF1BA7EC16EE44740' );
 ?column? | st_changeedgegeom 
----------+-------------------
 T13.2    | Edge 29 changed
(1 row)

-- Now add an obstacle and try to change back (should fail)
SELECT 'T13.3', TopoGeo_AddPoint('city_data', 'POINT(-1.1697 47.7825)'::geometry);
 ?column? | topogeo_addpoint 
----------+------------------
 T13.3    |               26
(1 row)

SELECT 'T13.4', ST_ChangeEdgeGeom('city_data', 29, '01020000001D000000E42CEC69873FF2BF9E98F56228E347400EDB16653648F2BF4985B18520E34740E92B4833164DF2BF3A1E335019E34740A94D9CDCEF50F2BF33F9669B1BE347407DAEB6627F59F2BF2CF180B229E34740758E01D9EB5DF2BFD0D556EC2FE34740533F6F2A5261F2BFD717096D39E34740F4893C49BA66F2BFC8073D9B55E34740B8239C16BC68F2BF33A7CB6262E34740AA2B9FE57970F2BF4165FCFB8CE347406DC5FEB27B72F2BFBA4E232D95E34740978BF84ECC7AF2BF24EEB1F4A1E34740E527D53E1D8FF2BF8F8D40BCAEE3474036CD3B4ED191F2BF649291B3B0E34740841266DAFE95F2BF1DE6CB0BB0E34740E3361AC05BA0F2BFB2632310AFE347405C5A0D897BACF2BF72F90FE9B7E3474031D3F6AFACB4F2BF4F232D95B7E347402B137EA99FB7F2BFD656EC2FBBE347402D431CEBE2B6F2BF551344DD07E4474011E4A08499B6F2BF15E3FC4D28E447406519E25817B7F2BF63EE5A423EE447409DD7D825AAB7F2BFE3FC4D2844E447405969520ABABDF2BF2384471B47E44740A31EA2D11DC4F2BFB1F9B83654E447400473F4F8BDCDF2BFEA5BE67459E447405070B1A206D3F2BFF19D98F562E4474062670A9DD7D8F2BF0E4FAF9465E447407FF6234564D8F2BFF1BA7EC16EE44740' );
ERROR:  Edge motion collision at POINT(-1.1697 47.7825)
-- See https://trac.osgeo.org/postgis/ticket/4706
DO $$
BEGIN
  -- 1. corrupt topology
  UPDATE city_data.edge_data SET right_face = 3 WHERE edge_id = 19;
  -- 2. Try to change the edge of a face with no bbox
  BEGIN
    SELECT ST_ChangeEdgeGeom('city_data', 7, 'LINESTRING(21 22,28 20,35 22)' );
  EXCEPTION
  WHEN OTHERS THEN
    -- Strip details, we only want the first part
    RAISE EXCEPTION '%', regexp_replace(SQLERRM, '([^:]): .*', '\1 (see #4706)');
  END;
END;
$$ LANGUAGE 'plpgsql';
ERROR:  Corrupted topolog1 (see #4706)
--
---- TODO: test changing some clockwise closed edges..
--
SELECT topology.DropTopology('city_data');
         droptopology         
------------------------------
 Topology 'city_data' dropped
(1 row)

