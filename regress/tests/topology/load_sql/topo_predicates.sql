










-- This scripts add some features to the ones defined by load_topology.sql
-- and then runs some binary predicates on them.
--


BEGIN;


-- Detect intersections between traffic_signs

SELECT 'POINT/POINT INTERSECTS' as operation;

SELECT a.feature_name, b.feature_name FROM
	features.traffic_signs a, features.traffic_signs b
	WHERE a.fid < b.fid AND topology.intersects(a.feature, b.feature);



-- Detect intersections between city_streets and traffic_signs

SELECT 'POINT/LINE INTERSECTS' as operation;

SELECT a.feature_name, b.feature_name FROM
	features.traffic_signs a, features.city_streets b
	WHERE topology.intersects(a.feature, b.feature);



-- Detect intersections between city_streets

SELECT 'LINE/LINE INTERSECTS' as operation;

SELECT a.feature_name, b.feature_name FROM
	features.city_streets a, features.city_streets b
	WHERE a.fid < b.fid AND topology.intersects(a.feature, b.feature);


-- Detect intersections between traffic_signs and land_parcels

SELECT 'POINT/POLY INTERSECTS' as operation;

SELECT a.feature_name, b.feature_name FROM
	features.traffic_signs a, features.land_parcels b
	WHERE topology.intersects(a.feature, b.feature);


-- Detect intersections between city_streets and land_parcels

SELECT 'LINE/POLY INTERSECTS' as operation;

SELECT a.feature_name, b.feature_name FROM
	features.city_streets a, features.land_parcels b
	WHERE topology.intersects(a.feature, b.feature);


-- Detect intersections between land_parcels and land_parcels

SELECT 'POLY/POLY INTERSECTS' as operation;

SELECT a.feature_name, b.feature_name FROM
	features.land_parcels a, features.land_parcels b
	WHERE a.fid < b.fid AND topology.intersects(a.feature, b.feature);


SELECT 'POINT/POINT EQUALS' as operation;

SELECT a.feature_name, b.feature_name FROM
	features.traffic_signs a, features.traffic_signs b
	WHERE a.fid < b.fid AND topology.equals(a.feature, b.feature);



SELECT 'LINE/LINE EQUALS' as operation;

SELECT a.feature_name, b.feature_name FROM
	features.city_streets a, features.city_streets b
	WHERE a.fid < b.fid AND topology.equals(a.feature, b.feature);



SELECT 'POLYGON/POLYGON EQUALS' as operation;

SELECT a.feature_name, b.feature_name FROM
	features.land_parcels a, features.land_parcels b
	WHERE a.fid < b.fid AND topology.equals(a.feature, b.feature);


SELECT 'POINT/POINT EQUALS (simple/hierarchical)' as operation;

SELECT a.feature_name, b.feature_name
	FROM features.traffic_signs a, features.big_signs b
	WHERE a.fid < b.fid AND topology.equals(a.feature, b.feature)
	ORDER BY a.feature_name;

SELECT 'POLYGON/POLYGON EQUALS (simple/hierarchical)' as operation;

SELECT a.feature_name, b.feature_name
	FROM features.land_parcels a, features.big_parcels b
	WHERE a.fid < b.fid AND topology.equals(a.feature, b.feature)
	ORDER BY a.feature_name;

END;

