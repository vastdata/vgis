\set VERBOSITY terse
set client_min_messages to ERROR;
SET
-- Import city_data
\i ./load_sql/load_topology-4326.sql
--
-- From examples in chapter 1.12.1 of
-- "Spatial Topology and Network Data Models" (Oracle manual)
--
-- Modified to use postgis-based topology model.
-- Loads the whole topology represented in Figure 1-1 of the
-- manual
--
--ORA---------------------------------------------------------------------
--ORA---- Main steps for using the topology data model with a topology
--ORA---- built from edge, node, and face data
--ORA---------------------------------------------------------------------
--ORA---- 1. Create a topology.
--ORA---- 2. Load (normally bulk-load) topology data
--ORA----    (node, edge, and face tables).
BEGIN;
BEGIN
-- 1. Create the topology.
--
-- NOTE:
--  Returns topology id... which depend on how many
--  topologies where created in the regress database
--  so we just check it is a number greater than 0
--
SELECT NULL FROM topology.CreateTopology('city_data', 4326);
 ?column? 
----------
 
(1 row)

-- 2. Load topology data (node, edge, and face tables).
-- Use INSERT statements here instead of a bulk-load utility.
-- 2A. Insert data into <topology_name>.FACE table.
INSERT INTO city_data.face(face_id) VALUES(1); -- F1
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(2); -- F2
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(3); -- F3
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(4); -- F4
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(5); -- F5
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(6); -- F6
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(7); -- F7
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(8); -- F8
INSERT 0 1
INSERT INTO city_data.face(face_id) VALUES(9); -- F9
INSERT 0 1
update pg_depend set deptype = 'z' where deptype = 'a';
UPDATE 414
-- UPDATE Face id sequence
SELECT NULL FROM setval('city_data.face_face_id_seq', 9);
 ?column? 
----------
 
(1 row)

-- 2B. Insert data into <topology_name>.NODE table.
-- N1
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(1, 'SRID=4326;POINT(8 30)', NULL);
INSERT 0 1
-- N2
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(2, 'SRID=4326;POINT(25 30)', NULL);
INSERT 0 1
-- N3
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(3, 'SRID=4326;POINT(25 35)', NULL);
INSERT 0 1
-- N4
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(4, 'SRID=4326;POINT(20 37)', 2);
INSERT 0 1
-- N5
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(5, 'SRID=4326;POINT(36 38)', NULL);
INSERT 0 1
-- N6
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(6, 'SRID=4326;POINT(57 33)', NULL);
INSERT 0 1
-- N7
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(7, 'SRID=4326;POINT(41 40)', NULL);
INSERT 0 1
-- N8
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(8, 'SRID=4326;POINT(9 6)', NULL);
INSERT 0 1
-- N9
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(9, 'SRID=4326;POINT(21 6)', NULL);
INSERT 0 1
-- N10
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(10, 'SRID=4326;POINT(35 6)', NULL);
INSERT 0 1
-- N11
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(11, 'SRID=4326;POINT(47 6)', NULL);
INSERT 0 1
-- N12
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(12, 'SRID=4326;POINT(47 14)', NULL);
INSERT 0 1
-- N13
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(13, 'SRID=4326;POINT(35 14)', NULL);
INSERT 0 1
-- N14
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(14, 'SRID=4326;POINT(21 14)', NULL);
INSERT 0 1
-- N15
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(15, 'SRID=4326;POINT(9 14)', NULL);
INSERT 0 1
-- N16
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(16, 'SRID=4326;POINT(9 22)', NULL);
INSERT 0 1
-- N17
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(17, 'SRID=4326;POINT(21 22)', NULL);
INSERT 0 1
-- N18
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(18, 'SRID=4326;POINT(35 22)', NULL);
INSERT 0 1
-- N19
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(19, 'SRID=4326;POINT(47 22)', NULL);
INSERT 0 1
-- N20
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(20, 'SRID=4326;POINT(4 31)', NULL);
INSERT 0 1
-- N21
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(21, 'SRID=4326;POINT(9 35)', NULL);
INSERT 0 1
-- N22
INSERT INTO city_data.node(node_id, geom, containing_face)
	VALUES(22, 'SRID=4326;POINT(13 35)', NULL);
INSERT 0 1
-- UPDATE Node id sequence
SELECT NULL FROM setval('city_data.node_node_id_seq', 22);
 ?column? 
----------
 
(1 row)

-- 2C. Insert data into <topology_name>.EDGE table.
-- E1
INSERT INTO city_data.edge VALUES(1, 1, 1, 1, -1, 1, 0,
  'SRID=4326;LINESTRING(8 30, 16 30, 16 38, 3 38, 3 30, 8 30)');
INSERT 0 1
-- E2
INSERT INTO city_data.edge VALUES(2, 2, 2, 3, -2, 2, 0,
  'SRID=4326;LINESTRING(25 30, 31 30, 31 40, 17 40, 17 30, 25 30)');
INSERT 0 1
-- E3
INSERT INTO city_data.edge VALUES(3, 2, 3, -3, 2, 2, 2,
  'SRID=4326;LINESTRING(25 30, 25 35)');
INSERT 0 1
-- E4
INSERT INTO city_data.edge VALUES(4, 5, 6, -5, 4, 0, 0,
    'SRID=4326;LINESTRING(36 38, 38 35, 41 34, 42 33, 45 32, 47 28, 50 28, 52 32, 57 33)');
INSERT 0 1
-- E5
INSERT INTO city_data.edge VALUES(5, 7, 6, -4, 5, 0, 0,
    'SRID=4326;LINESTRING(41 40, 45 40, 47 42, 62 41, 61 38, 59 39, 57 36, 57 33)');
INSERT 0 1
-- E6
INSERT INTO city_data.edge VALUES(6, 16, 17, 7, -21, 0, 3,
    'SRID=4326;LINESTRING(9 22, 21 22)');
INSERT 0 1
-- E7
INSERT INTO city_data.edge VALUES(7, 17, 18, 8, -19, 0, 4,
    'SRID=4326;LINESTRING(21 22, 35 22)');
INSERT 0 1
-- E8
INSERT INTO city_data.edge VALUES(8, 18, 19, -15, -17, 0, 5,
    'SRID=4326;LINESTRING(35 22, 47 22)');
INSERT 0 1
-- E9
INSERT INTO city_data.edge VALUES(9, 15, 14, 19, -22, 3, 6,
    'SRID=4326;LINESTRING(9 14, 21 14)');
INSERT 0 1
-- E10
INSERT INTO city_data.edge VALUES(10, 13, 14, -20, 17, 7, 4,
    'SRID=4326;LINESTRING(35 14, 21 14)');
INSERT 0 1
-- E11
INSERT INTO city_data.edge VALUES(11, 13, 12, 15, -18, 5, 8,
    'SRID=4326;LINESTRING(35 14, 47 14)');
INSERT 0 1
-- E12
INSERT INTO city_data.edge VALUES(12, 8, 9, 20, 22, 6, 0,
    'SRID=4326;LINESTRING(9 6, 21 6)');
INSERT 0 1
-- E13
INSERT INTO city_data.edge VALUES(13, 9, 10, 18, -12, 7, 0,
    'SRID=4326;LINESTRING(21 6, 35 6)');
INSERT 0 1
-- E14
INSERT INTO city_data.edge VALUES(14, 10, 11, 16, -13, 8, 0,
    'SRID=4326;LINESTRING(35 6, 47 6)');
INSERT 0 1
-- E15
INSERT INTO city_data.edge VALUES(15, 12, 19, -8, -16, 5, 0,
    'SRID=4326;LINESTRING(47 14, 47 22)');
INSERT 0 1
-- E16
INSERT INTO city_data.edge VALUES(16, 11, 12, -11, -14, 8, 0,
    'SRID=4326;LINESTRING(47 6, 47 14)');
INSERT 0 1
-- E17
INSERT INTO city_data.edge VALUES(17, 13, 18, -7, 11, 4, 5,
    'SRID=4326;LINESTRING(35 14, 35 22)');
INSERT 0 1
-- E18
INSERT INTO city_data.edge VALUES(18, 10, 13, 10, 14, 7, 8,
    'SRID=4326;LINESTRING(35 6, 35 14)');
INSERT 0 1
-- E19
INSERT INTO city_data.edge VALUES(19, 14, 17, -6, -10, 3, 4,
    'SRID=4326;LINESTRING(21 14, 21 22)');
INSERT 0 1
-- E20
INSERT INTO city_data.edge VALUES(20, 9, 14, -9, 13, 6, 7,
    'SRID=4326;LINESTRING(21 6, 21 14)');
INSERT 0 1
-- E21
INSERT INTO city_data.edge VALUES(21, 15, 16, 6, 9, 0, 3,
    'SRID=4326;LINESTRING(9 14, 9 22)');
INSERT 0 1
-- E22
INSERT INTO city_data.edge VALUES(22, 8, 15, 21, 12, 0, 6,
    'SRID=4326;LINESTRING(9 6, 9 14)');
INSERT 0 1
-- E25
INSERT INTO city_data.edge VALUES(25, 21, 22, -25, 25, 1, 1,
  'SRID=4326;LINESTRING(9 35, 13 35)');
INSERT 0 1
-- E26
INSERT INTO city_data.edge VALUES(26, 20, 20, 26, -26, 9, 1,
  'SRID=4326;LINESTRING(4 31, 7 31, 7 34, 4 34, 4 31)');
INSERT 0 1
-- UPDATE Edge id sequence
SELECT NULL FROM setval('city_data.edge_data_edge_id_seq', 26);
 ?column? 
----------
 
(1 row)

-- Set face minimum bounding rectangle
UPDATE city_data.face set mbr = ST_SetSRID( ( select st_extent(geom) from city_data.edge where left_face = face_id or right_face = face_id ), 4326 ) where face_id != 0;
UPDATE 9
update pg_depend set deptype = 'a' where deptype = 'z';
UPDATE 414
END;
COMMIT
-- Save max node id
select 'node'::text as what, max(node_id) INTO city_data.limits FROM city_data.node;
INSERT 0 1
INSERT INTO city_data.limits select 'edge'::text as what, max(edge_id) FROM city_data.edge;
INSERT 0 1
SELECT 'max',* from city_data.limits;
 ?column? | what | max 
----------+------+-----
 max      | node |  22
 max      | edge |  26
(2 rows)

-- Check changes since last saving, save more
-- {
CREATE OR REPLACE FUNCTION check_changes()
RETURNS TABLE (o text)
AS $$
DECLARE
  rec RECORD;
  sql text;
BEGIN
  -- Check effect on nodes
  sql := 'SELECT n.node_id, ''N|'' || n.node_id || ''|'' ||
        COALESCE(n.containing_face::text,'''') || ''|'' ||
        ST_AsText(ST_SnapToGrid(n.geom, 0.2))::text as xx
  	FROM city_data.node n WHERE n.node_id > (
    		SELECT max FROM city_data.limits WHERE what = ''node''::text )
  		ORDER BY n.node_id';

  FOR rec IN EXECUTE sql LOOP
    o := rec.xx;
    RETURN NEXT;
  END LOOP;

  -- Check effect on edges (there should be one split)
  sql := '
  WITH node_limits AS ( SELECT max FROM city_data.limits WHERE what = ''node''::text ),
       edge_limits AS ( SELECT max FROM city_data.limits WHERE what = ''edge''::text )
  SELECT ''E|'' || e.edge_id || ''|sn'' || e.start_node || ''|en'' || e.end_node
         || ''|nl'' || e.next_left_edge
         || ''|nr'' || e.next_right_edge
         || ''|lf'' || e.left_face
         || ''|rf'' || e.right_face
         :: text as xx
   FROM city_data.edge e, node_limits nl, edge_limits el
   WHERE e.start_node > nl.max
      OR e.end_node > nl.max
      OR e.edge_id > el.max
  ORDER BY e.edge_id;
  ';

  FOR rec IN EXECUTE sql LOOP
    o := rec.xx;
    RETURN NEXT;
  END LOOP;

  UPDATE city_data.limits SET max = (SELECT max(n.node_id) FROM city_data.node n) WHERE what = 'node';
  UPDATE city_data.limits SET max = (SELECT max(e.edge_id) FROM city_data.edge e) WHERE what = 'edge';

END;
$$ LANGUAGE 'plpgsql';
CREATE FUNCTION
-- }
-- Invalid calls
SELECT 'invalid', ST_NewEdgesSplit('city_data', 999, 'SRID=4326;POINT(36 26, 38 30)');
ERROR:  geometry has too many points at character 54
SELECT 'invalid', ST_NewEdgesSplit('city_data', 10, 'SRID=4326;POINT(28 15)');
ERROR:  SQL/MM Spatial exception - point not on edge
SELECT 'invalid', ST_NewEdgesSplit('', 10, 'SRID=4326;POINT(28 14)'); -- pg兼容性与其它兼容性的预期结果不同，pg兼容性预期结果如下注释
ERROR:  SQL/MM Spatial exception - null argument
-- ERROR:  SQL/MM Spatial exception - invalid topology name
SELECT 'invalid', ST_NewEdgesSplit(' ', 10, 'SRID=4326;POINT(28 14)');
ERROR:  SQL/MM Spatial exception - invalid topology name
SELECT 'invalid', ST_NewEdgesSplit(NULL, 10, 'SRID=4326;POINT(28 14)');
ERROR:  SQL/MM Spatial exception - null argument
SELECT 'invalid', ST_NewEdgesSplit('city_data', NULL, 'SRID=4326;POINT(28 14)');
ERROR:  SQL/MM Spatial exception - null argument
SELECT 'invalid', ST_NewEdgesSplit('city_data', 10, NULL);
ERROR:  SQL/MM Spatial exception - null argument
SELECT 'invalid', ST_NewEdgesSplit('fake', 10, 'SRID=4326;POINT(28 14)');
ERROR:  SQL/MM Spatial exception - invalid topology name
-- Non-isolated edge
SELECT 'noniso', ST_NewEdgesSplit('city_data', 10, 'SRID=4326;POINT(28 14)');
 ?column? | st_newedgessplit 
----------+------------------
 noniso   |               23
(1 row)

SELECT check_changes();
           check_changes            
------------------------------------
 N|23||POINT(28 14)
 E|27|sn13|en23|nl28|nr17|lf7|rf4
 E|28|sn23|en14|nl-20|nr-27|lf7|rf4
(3 rows)

-- Isolated edge
SELECT 'iso', ST_NewEdgesSplit('city_data', 25, 'SRID=4326;POINT(11 35)');
 ?column? | st_newedgessplit 
----------+------------------
 iso      |               24
(1 row)

SELECT check_changes();
           check_changes            
------------------------------------
 N|24||POINT(11 35)
 E|29|sn21|en24|nl30|nr29|lf1|rf1
 E|30|sn24|en22|nl-30|nr-29|lf1|rf1
(3 rows)

-- Dangling on end point
SELECT 'dangling_end', ST_NewEdgesSplit('city_data', 3, 'SRID=4326;POINT(25 32)');
   ?column?   | st_newedgessplit 
--------------+------------------
 dangling_end |               25
(1 row)

SELECT check_changes();
           check_changes           
-----------------------------------
 N|25||POINT(25 32)
 E|31|sn2|en25|nl32|nr2|lf2|rf2
 E|32|sn25|en3|nl-32|nr-31|lf2|rf2
(3 rows)

-- Dangling on start point
SELECT 'dangling_start', ST_NewEdgesSplit('city_data', 4, 'SRID=4326;POINT(45 32)');
    ?column?    | st_newedgessplit 
----------------+------------------
 dangling_start |               26
(1 row)

SELECT check_changes();
          check_changes           
----------------------------------
 N|26||POINT(45 32)
 E|33|sn5|en26|nl34|nr33|lf0|rf0
 E|34|sn26|en6|nl-5|nr-33|lf0|rf0
(3 rows)

-- Splitting closed edge
SELECT 'closed', ST_NewEdgesSplit('city_data', 1, 'SRID=4326;POINT(3 38)');
 ?column? | st_newedgessplit 
----------+------------------
 closed   |               27
(1 row)

SELECT check_changes();
          check_changes           
----------------------------------
 N|27||POINT(3 38)
 E|35|sn1|en27|nl36|nr-36|lf1|rf0
 E|36|sn27|en1|nl35|nr-35|lf1|rf0
(3 rows)

--
-- Split an edge referenced by multiple TopoGeometries
--
-- See https://trac.osgeo.org/postgis/ticket/3407
--
CREATE TABLE city_data.fl(id varchar);
CREATE TABLE
SELECT 'L' || topology.AddTopoGeometryColumn('city_data',
  'city_data', 'fl', 'g', 'LINESTRING');
 ?column? 
----------
 L1
(1 row)

INSERT INTO city_data.fl VALUES
 ('E7.1', topology.CreateTopoGeom('city_data', 2, 1, '{{7,2}}')),
 ('E7.2', topology.CreateTopoGeom('city_data', 2, 1, '{{7,2}}'));
INSERT 0 2
SELECT '#3407', ST_ModEdgeSplit('city_data', 7, 'SRID=4326;POINT(28 22)');
 ?column? | st_modedgesplit 
----------+-----------------
 #3407    |              28
(1 row)

SELECT check_changes();
          check_changes           
----------------------------------
 N|28||POINT(28 22)
 E|7|sn17|en28|nl37|nr-19|lf0|rf4
 E|37|sn28|en18|nl8|nr-7|lf0|rf4
(3 rows)

-- Robustness of edge splitting (#1711)
-- clean all up first
DELETE FROM city_data.edge_data;
DELETE 30
DELETE FROM city_data.node;
DELETE 28
DELETE FROM city_data.face where face_id > 0;
DELETE 9
update pg_depend set deptype = 'z' where deptype = 'a';
UPDATE 419
SELECT 'seq_reset',
       setval('city_data.edge_data_edge_id_seq', 1, false),
       setval('city_data.face_face_id_seq', 1, false),
       setval('city_data.node_node_id_seq', 1, false);
 ?column?  | setval | setval | setval 
-----------+--------+--------+--------
 seq_reset |      1 |      1 |      1
(1 row)

update pg_depend set deptype = 'a' where deptype = 'z';
UPDATE 419
CREATE TEMP TABLE t AS
SELECT
ST_SetSRID(
'01020000000400000000000000000034400000000000002440000000000000244000000000000024400000000000002240000000000000284000000000000024400000000000003440'
::geometry, 4326) as line,
ST_SetSRID(
'010100000000000000000022400000000000002840'
::geometry, 4326) as point,
null::int as edge_id,
null::int as node_id
;
INSERT 0 1
UPDATE t SET edge_id = AddEdge('city_data', line);
UPDATE 1
UPDATE t SET node_id = ST_NewEdgesSplit('city_data', t.edge_id, t.point);
UPDATE 1
SELECT 'robust.1', 'E'||edge_id, 'N'||node_id FROM t;
 ?column? | ?column? | ?column? 
----------+----------+----------
 robust.1 | E1       | N3
(1 row)

SELECT check_changes();
 check_changes 
---------------
(0 rows)

SELECT 'robust.2',
 ST_Equals(t.point, ST_EndPoint(e1.geom)),
 ST_Equals(t.point, ST_StartPoint(e2.geom))
FROM t, city_data.edge e1, city_data.edge e2, city_data.node n
WHERE n.node_id = t.node_id
 AND e1.end_node = n.node_id
 AND e2.start_node = n.node_id;
 ?column? | st_equals | st_equals 
----------+-----------+-----------
 robust.2 | t         | t
(1 row)

DROP TABLE t;
DROP TABLE
DROP FUNCTION check_changes();
DROP FUNCTION
SELECT DropTopology('city_data');
         droptopology         
------------------------------
 Topology 'city_data' dropped
(1 row)

