set client_min_messages to WARNING;

-- Tests TopoGeometry->Geometry cast and id(TopoGeometry)

\i ./load_sql/load_topology.sql
\i ./load_sql/load_features.sql
\i ./load_sql/more_features.sql
\i ./load_sql/hierarchy.sql
\i ./load_sql/query_features.sql

-- clean up
SELECT topology.DropTopology('city_data');
DROP SCHEMA features CASCADE;
