DROP TABLE IF EXISTS raster_value_arrays;
NOTICE:  table "raster_value_arrays" does not exist, skipping
DROP TABLE
CREATE TABLE raster_value_arrays (
	id integer,
	val double precision[][]
);
CREATE TABLE
CREATE OR REPLACE FUNCTION make_value_array(
	rows integer DEFAULT 3,
	columns integer DEFAULT 3,
	start_val double precision DEFAULT 1,
	step double precision DEFAULT 1,
	skip_expr text DEFAULT NULL
)
	RETURNS double precision[][][]
	AS $$
	DECLARE
		x int;
		y int;
		_value double precision;
		values double precision[][][];
		result boolean;
		expr text;
	BEGIN
		_value := start_val;

		values := array_fill(NULL::double precision, ARRAY[1, columns, rows]);

		FOR y IN 1..columns LOOP
			FOR x IN 1..rows LOOP
				IF skip_expr IS NULL OR length(skip_expr) < 1 THEN
					result := TRUE;
				ELSE
					expr := replace(skip_expr, '[v]'::text, _value::text);
					EXECUTE 'SELECT (' || expr || ')::boolean' INTO result;
				END IF;

				IF result IS TRUE THEN
					values[1][y][x] := _value;
				END IF;

				_value := _value + step;
			END LOOP;
		END LOOP;

		RETURN values;
	END;
	$$ LANGUAGE 'plpgsql';
CREATE FUNCTION
INSERT INTO raster_value_arrays VALUES
	(1, make_value_array()),
	(2, make_value_array(5, 5)),
	(3, make_value_array(5, 5, 100)),
	(4, make_value_array(3, 3, 15, -1)),
	(5, make_value_array(5, 5, 15, -1)),
	(6, make_value_array(3, 3, 1, 2)),
	(7, make_value_array(5, 5, 1, 3)),
	(10, make_value_array(3, 3, 1, 1, '0')),
	(11, make_value_array(5, 5, 1, 1, '0')),
	(12, make_value_array(3, 3, 1, 1, '[v] % 2')),
	(13, make_value_array(5, 5, 1, 1, '[v] % 2')),
	(14, make_value_array(3, 3, 1, 1, '([v] % 2) = 0')),
	(15, make_value_array(5, 5, 1, 1, '([v] % 2) = 0')),
	(16, make_value_array(3, 3, 1, 2.1, '([v] NOT IN (7.3, 9.4, 15.7, 17.8))')),
	(17, make_value_array(3, 3, 0, 3.14, '([v] IN (3.14, 12.56, 25.12))'))
;
INSERT 0 15
SELECT
	id,
	val,
	round(st_distinct4ma(val, NULL)::float8, 6) AS distinct4ma,
	round(st_max4ma(val, NULL)::float8, 6) AS max4ma,
	round(st_mean4ma(val, NULL)::float8, 6) AS mean4ma,
	round(st_min4ma(val, NULL)::float8, 6) AS min4ma,
	round(st_range4ma(val, NULL)::float8, 6) AS range4ma,
	round(st_stddev4ma(val, NULL)::float8, 6) AS stddev4ma,
	round(st_sum4ma(val, NULL)::float8, 6) AS sum4ma
FROM raster_value_arrays
ORDER BY id;
 id |                                                                    val                                                                     | distinct4ma |   max4ma   |  mean4ma   |   min4ma   | range4ma  | stddev4ma |   sum4ma    
----+--------------------------------------------------------------------------------------------------------------------------------------------+-------------+------------+------------+------------+-----------+-----------+-------------
  1 | {{{1,2,3},{4,5,6},{7,8,9}}}                                                                                                                |    9.000000 |   9.000000 |   5.000000 |   1.000000 |  8.000000 |  2.738613 |   45.000000
  2 | {{{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}}}                                                            |   25.000000 |  25.000000 |  13.000000 |   1.000000 | 24.000000 |  7.359801 |  325.000000
  3 | {{{100,101,102,103,104},{105,106,107,108,109},{110,111,112,113,114},{115,116,117,118,119},{120,121,122,123,124}}}                          |   25.000000 | 124.000000 | 112.000000 | 100.000000 | 24.000000 |  7.359801 | 2800.000000
  4 | {{{15,14,13},{12,11,10},{9,8,7}}}                                                                                                          |    9.000000 |  15.000000 |  11.000000 |   7.000000 |  8.000000 |  2.738613 |   99.000000
  5 | {{{15,14,13,12,11},{10,9,8,7,6},{5,4,3,2,1},{0,-1,-2,-3,-4},{-5,-6,-7,-8,-9}}}                                                             |   25.000000 |  15.000000 |   3.000000 |  -9.000000 | 24.000000 |  7.359801 |   75.000000
  6 | {{{1,3,5},{7,9,11},{13,15,17}}}                                                                                                            |    9.000000 |  17.000000 |   9.000000 |   1.000000 | 16.000000 |  5.477226 |   81.000000
  7 | {{{1,4,7,10,13},{16,19,22,25,28},{31,34,37,40,43},{46,49,52,55,58},{61,64,67,70,73}}}                                                      |   25.000000 |  73.000000 |  37.000000 |   1.000000 | 72.000000 | 22.079402 |  925.000000
 10 | {{{NULL,NULL,NULL},{NULL,NULL,NULL},{NULL,NULL,NULL}}}                                                                                     |    0.000000 |            |            |            |           |           |    0.000000
 11 | {{{NULL,NULL,NULL,NULL,NULL},{NULL,NULL,NULL,NULL,NULL},{NULL,NULL,NULL,NULL,NULL},{NULL,NULL,NULL,NULL,NULL},{NULL,NULL,NULL,NULL,NULL}}} |    0.000000 |            |            |            |           |           |    0.000000
 12 | {{{1,NULL,3},{NULL,5,NULL},{7,NULL,9}}}                                                                                                    |    5.000000 |   9.000000 |   5.000000 |   1.000000 |  8.000000 |  3.162278 |   25.000000
 13 | {{{1,NULL,3,NULL,5},{NULL,7,NULL,9,NULL},{11,NULL,13,NULL,15},{NULL,17,NULL,19,NULL},{21,NULL,23,NULL,25}}}                                |   13.000000 |  25.000000 |  13.000000 |   1.000000 | 24.000000 |  7.788881 |  169.000000
 14 | {{{NULL,2,NULL},{4,NULL,6},{NULL,8,NULL}}}                                                                                                 |    4.000000 |   8.000000 |   5.000000 |   2.000000 |  6.000000 |  2.581989 |   20.000000
 15 | {{{NULL,2,NULL,4,NULL},{6,NULL,8,NULL,10},{NULL,12,NULL,14,NULL},{16,NULL,18,NULL,20},{NULL,22,NULL,24,NULL}}}                             |   12.000000 |  24.000000 |  13.000000 |   2.000000 | 22.000000 |  7.211103 |  156.000000
 16 | {{{1,3.1,5.2},{NULL,NULL,11.5},{13.6,NULL,NULL}}}                                                                                          |    5.000000 |  13.600000 |   6.880000 |   1.000000 | 12.600000 |  5.435715 |   34.400000
 17 | {{{NULL,3.14,NULL},{NULL,12.56,NULL},{NULL,NULL,25.12}}}                                                                                   |    3.000000 |  25.120000 |  13.606667 |   3.140000 | 21.980000 | 11.027318 |   40.820000
(15 rows)

DROP TABLE IF EXISTS raster_value_arrays;
DROP TABLE
DROP FUNCTION IF EXISTS make_value_array(integer, integer, double precision, double precision, text);
DROP FUNCTION
