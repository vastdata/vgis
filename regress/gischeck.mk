# postgis回归测试

GISRUNTESTFLAGS = --extension --raster --topology --sfcgal --tiger 
GISRUNTESTFLAGS += --clean # 清理结果目录
# GISRUNTESTFLAGS += --pg # 在pg兼容模式数据库中运行测试，兼容性隔离之后不支持使用该选项
# GISRUNTESTFLAGS += --mysql # 在mysql兼容模式数据库中运行测试，兼容性隔离之后不支持使用该选项
# GISRUNTESTFLAGS += --mssql # 在mssql兼容模式数据库中运行测试，兼容性隔离之后不支持使用该选项
# GISRUNTESTFLAGS += --expect # 覆盖期望结果加expect

# 添加gischeck回归测试文件
########################################################
GISTESTS += $(topsrcdir)/regress/tests/test_global_begin 

# postgis模块 #
GISTESTS += \
	$(topsrcdir)/regress/tests/postgis/affine \
	$(topsrcdir)/regress/tests/postgis/bestsrid \
	$(topsrcdir)/regress/tests/postgis/binary \
	$(topsrcdir)/regress/tests/postgis/boundary \
	$(topsrcdir)/regress/tests/postgis/chaikin \
	$(topsrcdir)/regress/tests/postgis/clean \
	$(topsrcdir)/regress/tests/postgis/clipbybox2d \
	$(topsrcdir)/regress/tests/postgis/cluster \
	$(topsrcdir)/regress/tests/postgis/computed_columns \
	$(topsrcdir)/regress/tests/postgis/concave_hull\
	$(topsrcdir)/regress/tests/postgis/concave_hull_hard \
	$(topsrcdir)/regress/tests/postgis/ctors \
	$(topsrcdir)/regress/tests/postgis/curvetoline \
	$(topsrcdir)/regress/tests/postgis/delaunaytriangles \
	$(topsrcdir)/regress/tests/postgis/dump \
	$(topsrcdir)/regress/tests/postgis/dumppoints \
	$(topsrcdir)/regress/tests/postgis/dumpsegments \
	$(topsrcdir)/regress/tests/postgis/empty \
	$(topsrcdir)/regress/tests/postgis/estimatedextent \
	$(topsrcdir)/regress/tests/postgis/filterm \
	$(topsrcdir)/regress/tests/postgis/fixedoverlay \
	$(topsrcdir)/regress/tests/postgis/flatgeobuf \
	$(topsrcdir)/regress/tests/postgis/forcecurve \
	$(topsrcdir)/regress/tests/postgis/frechet \
	$(topsrcdir)/regress/tests/postgis/geobuf \
	$(topsrcdir)/regress/tests/postgis/geography \
	$(topsrcdir)/regress/tests/postgis/geography_centroid \
	$(topsrcdir)/regress/tests/postgis/geography_covers \
	$(topsrcdir)/regress/tests/postgis/geometric_median \
  	$(topsrcdir)/regress/tests/postgis/geos_noop \
	$(topsrcdir)/regress/tests/postgis/geos38 \
	$(topsrcdir)/regress/tests/postgis/geos39 \
	$(topsrcdir)/regress/tests/postgis/geos310 \
	$(topsrcdir)/regress/tests/postgis/hausdorff \
	$(topsrcdir)/regress/tests/postgis/in_flatgeobuf \
	$(topsrcdir)/regress/tests/postgis/in_geohash \
	$(topsrcdir)/regress/tests/postgis/in_geojson \
	$(topsrcdir)/regress/tests/postgis/in_gml \
	$(topsrcdir)/regress/tests/postgis/in_kml \
	$(topsrcdir)/regress/tests/postgis/in_encodedpolyline \
	$(topsrcdir)/regress/tests/postgis/iscollection \
	$(topsrcdir)/regress/tests/postgis/isvaliddetail \
	$(topsrcdir)/regress/tests/postgis/legacy \
	$(topsrcdir)/regress/tests/postgis/long_xact \
	$(topsrcdir)/regress/tests/postgis/lwgeom_regress \
	$(topsrcdir)/regress/tests/postgis/measures \
	$(topsrcdir)/regress/tests/postgis/minimum_bounding_circle \
	$(topsrcdir)/regress/tests/postgis/minimum_clearance \
	$(topsrcdir)/regress/tests/postgis/mvt \
	$(topsrcdir)/regress/tests/postgis/node \
	$(topsrcdir)/regress/tests/postgis/normalize \
	$(topsrcdir)/regress/tests/postgis/offsetcurve \
	$(topsrcdir)/regress/tests/postgis/operators \
	$(topsrcdir)/regress/tests/postgis/orientation \
	$(topsrcdir)/regress/tests/postgis/oriented_envelope \
	$(topsrcdir)/regress/tests/postgis/out_flatgeobuf \
	$(topsrcdir)/regress/tests/postgis/out_geography \
	$(topsrcdir)/regress/tests/postgis/out_geometry \
	$(topsrcdir)/regress/tests/postgis/point_coordinates \
	$(topsrcdir)/regress/tests/postgis/polygonize \
	$(topsrcdir)/regress/tests/postgis/polyhedralsurface \
	$(topsrcdir)/regress/tests/postgis/postgis_type_name \
	$(topsrcdir)/regress/tests/postgis/quantize_coordinates \
	$(topsrcdir)/regress/tests/postgis/regress \
	$(topsrcdir)/regress/tests/postgis/regress_bdpoly \
	$(topsrcdir)/regress/tests/postgis/regress_buffer_params \
	$(topsrcdir)/regress/tests/postgis/regress_gist_index_nd \
	$(topsrcdir)/regress/tests/postgis/regress_index \
	$(topsrcdir)/regress/tests/postgis/regress_index_nulls \
	$(topsrcdir)/regress/tests/postgis/regress_lrs \
	$(topsrcdir)/regress/tests/postgis/regress_management \
	$(topsrcdir)/regress/tests/postgis/regress_ogc \
	$(topsrcdir)/regress/tests/postgis/regress_ogc_cover \
	$(topsrcdir)/regress/tests/postgis/regress_ogc_prep \
	$(topsrcdir)/regress/tests/postgis/regress_proj_basic \
	$(topsrcdir)/regress/tests/postgis/regress_proj_adhoc \
	$(topsrcdir)/regress/tests/postgis/regress_proj_cache_overflow \
	$(topsrcdir)/regress/tests/postgis/regress_proj_4890 \
	$(topsrcdir)/regress/tests/postgis/regress_selectivity \
	$(topsrcdir)/regress/tests/postgis/relate \
	$(topsrcdir)/regress/tests/postgis/relate_bnr \
	$(topsrcdir)/regress/tests/postgis/relatematch \
	$(topsrcdir)/regress/tests/postgis/remove_repeated_points \
	$(topsrcdir)/regress/tests/postgis/removepoint \
	$(topsrcdir)/regress/tests/postgis/reverse \
	$(topsrcdir)/regress/tests/postgis/scroll \
	$(topsrcdir)/regress/tests/postgis/setpoint \
	$(topsrcdir)/regress/tests/postgis/sharedpaths \
	$(topsrcdir)/regress/tests/postgis/simplify \
	$(topsrcdir)/regress/tests/postgis/simplifyvw \
	$(topsrcdir)/regress/tests/postgis/size \
	$(topsrcdir)/regress/tests/postgis/snap \
	$(topsrcdir)/regress/tests/postgis/snaptogrid \
	$(topsrcdir)/regress/tests/postgis/split \
	$(topsrcdir)/regress/tests/postgis/sql-mm-circularstring \
	$(topsrcdir)/regress/tests/postgis/sql-mm-compoundcurve \
	$(topsrcdir)/regress/tests/postgis/sql-mm-curvepoly \
	$(topsrcdir)/regress/tests/postgis/sql-mm-general \
	$(topsrcdir)/regress/tests/postgis/sql-mm-multicurve \
	$(topsrcdir)/regress/tests/postgis/sql-mm-multisurface \
	$(topsrcdir)/regress/tests/postgis/sql-mm-serialize \
	$(topsrcdir)/regress/tests/postgis/subdivide \
	$(topsrcdir)/regress/tests/postgis/summary \
	$(topsrcdir)/regress/tests/postgis/swapordinates \
	$(topsrcdir)/regress/tests/postgis/temporal \
	$(topsrcdir)/regress/tests/postgis/tickets \
	$(topsrcdir)/regress/tests/postgis/twkb \
	$(topsrcdir)/regress/tests/postgis/unaryunion \
	$(topsrcdir)/regress/tests/postgis/voronoi \
	$(topsrcdir)/regress/tests/postgis/wkb \
	$(topsrcdir)/regress/tests/postgis/wkt \
	$(topsrcdir)/regress/tests/postgis/wmsservers \
	$(topsrcdir)/regress/tests/postgis/wrapx \
	$(topsrcdir)/regress/tests/postgis/loader/Point \
	$(topsrcdir)/regress/tests/postgis/loader/PointM \
	$(topsrcdir)/regress/tests/postgis/loader/PointZ \
	$(topsrcdir)/regress/tests/postgis/loader/MultiPoint \
	$(topsrcdir)/regress/tests/postgis/loader/MultiPointM \
	$(topsrcdir)/regress/tests/postgis/loader/MultiPointZ \
	$(topsrcdir)/regress/tests/postgis/loader/Arc \
	$(topsrcdir)/regress/tests/postgis/loader/ArcM \
	$(topsrcdir)/regress/tests/postgis/loader/ArcZ \
	$(topsrcdir)/regress/tests/postgis/loader/Polygon \
	$(topsrcdir)/regress/tests/postgis/loader/PolygonM \
	$(topsrcdir)/regress/tests/postgis/loader/PolygonZ \
	$(topsrcdir)/regress/tests/postgis/loader/TSTPolygon \
	$(topsrcdir)/regress/tests/postgis/loader/TSIPolygon \
	$(topsrcdir)/regress/tests/postgis/loader/TSTIPolygon \
	$(topsrcdir)/regress/tests/postgis/loader/PointWithSchema \
	$(topsrcdir)/regress/tests/postgis/loader/NoTransPoint \
	$(topsrcdir)/regress/tests/postgis/loader/NotReallyMultiPoint \
	$(topsrcdir)/regress/tests/postgis/loader/MultiToSinglePoint \
	$(topsrcdir)/regress/tests/postgis/loader/ReprojectPts \
	$(topsrcdir)/regress/tests/postgis/loader/ReprojectPtsD \
	$(topsrcdir)/regress/tests/postgis/loader/ReprojectPtsGeog \
	$(topsrcdir)/regress/tests/postgis/loader/ReprojectPtsGeogD \
	$(topsrcdir)/regress/tests/postgis/loader/Latin1 \
	$(topsrcdir)/regress/tests/postgis/loader/Latin1-implicit \
	$(topsrcdir)/regress/tests/postgis/loader/mfile \
	$(topsrcdir)/regress/tests/postgis/loader/TestSkipANALYZE \
	$(topsrcdir)/regress/tests/postgis/loader/TestANALYZE \
	$(topsrcdir)/regress/tests/postgis/dumper/mfiledmp \
	$(topsrcdir)/regress/tests/postgis/dumper/literalsrid \
	$(topsrcdir)/regress/tests/postgis/dumper/realtable \
	$(topsrcdir)/regress/tests/postgis/dumper/nullsintable \
	$(topsrcdir)/regress/tests/postgis/dumper/null3d \
	$(topsrcdir)/regress/tests/postgis/dumper/withclause \

# raster模块 #
GISTESTS += \
	$(topsrcdir)/regress/tests/raster/load_outdb \
	$(topsrcdir)/regress/tests/raster/check_raster_overviews \
	$(topsrcdir)/regress/tests/raster/rt_io \
	$(topsrcdir)/regress/tests/raster/rt_bytea \
	$(topsrcdir)/regress/tests/raster/rt_wkb \
	$(topsrcdir)/regress/tests/raster/box3d \
	$(topsrcdir)/regress/tests/raster/rt_addband \
	$(topsrcdir)/regress/tests/raster/rt_asraster \
	$(topsrcdir)/regress/tests/raster/rt_band \
	$(topsrcdir)/regress/tests/raster/rt_tile \
	$(topsrcdir)/regress/tests/raster/rt_dimensions \
	$(topsrcdir)/regress/tests/raster/rt_dumpvalues \
	$(topsrcdir)/regress/tests/raster/rt_elevation_functions \
	$(topsrcdir)/regress/tests/raster/rt_clip \
	$(topsrcdir)/regress/tests/raster/rt_intersection \
	$(topsrcdir)/regress/tests/raster/rt_scale \
	$(topsrcdir)/regress/tests/raster/rt_pixelsize \
	$(topsrcdir)/regress/tests/raster/rt_upperleft \
	$(topsrcdir)/regress/tests/raster/rt_rotation \
	$(topsrcdir)/regress/tests/raster/rt_gdalwarp \
	$(topsrcdir)/regress/tests/raster/rt_georeference \
	$(topsrcdir)/regress/tests/raster/rt_geos_relationships \
	$(topsrcdir)/regress/tests/raster/rt_set_properties \
	$(topsrcdir)/regress/tests/raster/rt_isempty \
	$(topsrcdir)/regress/tests/raster/rt_hasnoband \
	$(topsrcdir)/regress/tests/raster/rt_metadata \
	$(topsrcdir)/regress/tests/raster/rt_rastertoworldcoord \
	$(topsrcdir)/regress/tests/raster/rt_worldtorastercoord \
	$(topsrcdir)/regress/tests/raster/rt_convexhull \
	$(topsrcdir)/regress/tests/raster/rt_envelope \
	$(topsrcdir)/regress/tests/raster/rt_band_properties \
	$(topsrcdir)/regress/tests/raster/rt_set_band_properties \
	$(topsrcdir)/regress/tests/raster/rt_pixelaspolygons \
	$(topsrcdir)/regress/tests/raster/rt_pixelaspoints \
	$(topsrcdir)/regress/tests/raster/rt_pixelascentroids \
	$(topsrcdir)/regress/tests/raster/rt_setvalues_array \
	$(topsrcdir)/regress/tests/raster/rt_summarystats \
	$(topsrcdir)/regress/tests/raster/rt_count \
	$(topsrcdir)/regress/tests/raster/rt_histogram \
	$(topsrcdir)/regress/tests/raster/rt_intersects \
	$(topsrcdir)/regress/tests/raster/rt_quantile \
	$(topsrcdir)/regress/tests/raster/rt_valuecount \
	$(topsrcdir)/regress/tests/raster/rt_valuepercent \
	$(topsrcdir)/regress/tests/raster/rt_bandmetadata \
	$(topsrcdir)/regress/tests/raster/rt_pixelvalue \
	$(topsrcdir)/regress/tests/raster/rt_neighborhood \
	$(topsrcdir)/regress/tests/raster/rt_pixelofvalue \
	$(topsrcdir)/regress/tests/raster/rt_polygon \
	$(topsrcdir)/regress/tests/raster/rt_setbandpath \
	$(topsrcdir)/regress/tests/raster/rt_utility \
	$(topsrcdir)/regress/tests/raster/rt_fromgdalraster \
	$(topsrcdir)/regress/tests/raster/rt_asgdalraster \
	$(topsrcdir)/regress/tests/raster/rt_astiff \
	$(topsrcdir)/regress/tests/raster/rt_asjpeg \
	$(topsrcdir)/regress/tests/raster/rt_aspng \
	$(topsrcdir)/regress/tests/raster/rt_reclass \
	$(topsrcdir)/regress/tests/raster/rt_gdalcontour \
	$(topsrcdir)/regress/tests/raster/rt_makeemptycoverage \
	$(topsrcdir)/regress/tests/raster/rt_mapalgebraexpr \
	$(topsrcdir)/regress/tests/raster/rt_mapalgebrafct \
	$(topsrcdir)/regress/tests/raster/rt_mapalgebrafctngb \
	$(topsrcdir)/regress/tests/raster/rt_mapalgebrafctngb_userfunc \
	$(topsrcdir)/regress/tests/raster/rt_mapalgebra \
	$(topsrcdir)/regress/tests/raster/rt_mapalgebra_mask \
	$(topsrcdir)/regress/tests/raster/rt_mapalgebra_expr \
	$(topsrcdir)/regress/tests/raster/rt_mapalgebraexpr_2raster \
	$(topsrcdir)/regress/tests/raster/rt_mapalgebrafct_2raster \
	$(topsrcdir)/regress/tests/raster/rt_union \
	$(topsrcdir)/regress/tests/raster/rt_invdistweight4ma \
	$(topsrcdir)/regress/tests/raster/rt_4ma \
	$(topsrcdir)/regress/tests/raster/rt_setvalues_geomval \
	$(topsrcdir)/regress/tests/raster/rt_colormap \
	$(topsrcdir)/regress/tests/raster/rt_grayscale \
	$(topsrcdir)/regress/tests/raster/rt_gist_relationships \
	$(topsrcdir)/regress/tests/raster/rt_samealignment \
	$(topsrcdir)/regress/tests/raster/rt_iscoveragetile \
	$(topsrcdir)/regress/tests/raster/bug_test_car5 \
	$(topsrcdir)/regress/tests/raster/tickets \
	$(topsrcdir)/regress/tests/raster/rt_nearestvalue \
	$(topsrcdir)/regress/tests/raster/rt_createoverview \
	$(topsrcdir)/regress/tests/raster/loader/Basic \
	$(topsrcdir)/regress/tests/raster/loader/Projected \
	$(topsrcdir)/regress/tests/raster/loader/BasicCopy \
	$(topsrcdir)/regress/tests/raster/loader/BasicFilename \
	$(topsrcdir)/regress/tests/raster/loader/BasicOutDB \
	$(topsrcdir)/regress/tests/raster/loader/Tiled10x10 \
	$(topsrcdir)/regress/tests/raster/loader/Tiled10x10Copy \
	$(topsrcdir)/regress/tests/raster/loader/Tiled8x8 \
	$(topsrcdir)/regress/tests/raster/loader/TiledAuto \
	$(topsrcdir)/regress/tests/raster/loader/TiledAutoSkipNoData\
	$(topsrcdir)/regress/tests/raster/clean \

# topology模块 #
GISTESTS += \
    $(topsrcdir)/regress/tests/topology/addedge \
	$(topsrcdir)/regress/tests/topology/addface2.5d \
    $(topsrcdir)/regress/tests/topology/addface \
	$(topsrcdir)/regress/tests/topology/addnode \
	$(topsrcdir)/regress/tests/topology/addtopogeometrycolumn \
	$(topsrcdir)/regress/tests/topology/copytopology \
	$(topsrcdir)/regress/tests/topology/cleartopogeom \
	$(topsrcdir)/regress/tests/topology/createtopogeom \
	$(topsrcdir)/regress/tests/topology/createtopology \
	$(topsrcdir)/regress/tests/topology/droptopogeometrycolumn \
	$(topsrcdir)/regress/tests/topology/droptopology \
	$(topsrcdir)/regress/tests/topology/findtopology \
	$(topsrcdir)/regress/tests/topology/findlayer \
	$(topsrcdir)/regress/tests/topology/geometry_cast \
	$(topsrcdir)/regress/tests/topology/getedgebypoint \
	$(topsrcdir)/regress/tests/topology/getfacebypoint \
	$(topsrcdir)/regress/tests/topology/getfacecontainingpoint \
	$(topsrcdir)/regress/tests/topology/getnodebypoint \
	$(topsrcdir)/regress/tests/topology/getnodeedges \
	$(topsrcdir)/regress/tests/topology/getringedges \
	$(topsrcdir)/regress/tests/topology/gettopogeomelements \
	$(topsrcdir)/regress/tests/topology/gml \
	$(topsrcdir)/regress/tests/topology/layertrigger \
	$(topsrcdir)/regress/tests/topology/legacy_invalid \
	$(topsrcdir)/regress/tests/topology/legacy_predicate \
	$(topsrcdir)/regress/tests/topology/legacy_query \
	$(topsrcdir)/regress/tests/topology/legacy_validate \
	$(topsrcdir)/regress/tests/topology/polygonize \
	$(topsrcdir)/regress/tests/topology/sqlmm \
	$(topsrcdir)/regress/tests/topology/st_addedgemodface \
	$(topsrcdir)/regress/tests/topology/st_addedgenewfaces \
	$(topsrcdir)/regress/tests/topology/st_addisoedge \
	$(topsrcdir)/regress/tests/topology/st_addisonode \
	$(topsrcdir)/regress/tests/topology/st_changeedgegeom \
	$(topsrcdir)/regress/tests/topology/st_createtopogeo \
	$(topsrcdir)/regress/tests/topology/st_getfaceedges \
	$(topsrcdir)/regress/tests/topology/st_getfacegeometry \
	$(topsrcdir)/regress/tests/topology/st_modedgeheal \
	$(topsrcdir)/regress/tests/topology/st_modedgesplit \
	$(topsrcdir)/regress/tests/topology/st_moveisonode \
	$(topsrcdir)/regress/tests/topology/st_newedgeheal \
	$(topsrcdir)/regress/tests/topology/st_newedgessplit \
	$(topsrcdir)/regress/tests/topology/st_remedgemodface \
	$(topsrcdir)/regress/tests/topology/st_remedgenewface \
	$(topsrcdir)/regress/tests/topology/st_removeisoedge \
	$(topsrcdir)/regress/tests/topology/st_removeisonode \
	$(topsrcdir)/regress/tests/topology/st_simplify \
	$(topsrcdir)/regress/tests/topology/topo2.5d \
	$(topsrcdir)/regress/tests/topology/topoelementarray_agg \
	$(topsrcdir)/regress/tests/topology/topoelement \
	$(topsrcdir)/regress/tests/topology/topogeo_addlinestring \
	$(topsrcdir)/regress/tests/topology/topogeo_addpoint \
	$(topsrcdir)/regress/tests/topology/topogeo_addpolygon \
    $(topsrcdir)/regress/tests/topology/topogeom_edit \
    $(topsrcdir)/regress/tests/topology/topogeom_addtopogeom \
	$(topsrcdir)/regress/tests/topology/topogeometry_srid \
	$(topsrcdir)/regress/tests/topology/topogeometry_type \
	$(topsrcdir)/regress/tests/topology/topojson \
    $(topsrcdir)/regress/tests/topology/topologysummary \
	$(topsrcdir)/regress/tests/topology/totopogeom \
	$(topsrcdir)/regress/tests/topology/validatetopologyrelation \

# sfcgal模块 #
GISTESTS += \
	$(topsrcdir)/regress/tests/sfcgal/regress_sfcgal \
	$(topsrcdir)/regress/tests/sfcgal/approximatemedialaxis

# 其它模块 #
GISTESTS += \
	$(topsrcdir)/regress/tests/extras/extra_new_add_tests \
	$(topsrcdir)/regress/tests/extras/normalize_address \
	$(topsrcdir)/regress/tests/extras/pagc_normalize_address \
	$(topsrcdir)/regress/tests/extras/parseaddress \
	$(topsrcdir)/regress/tests/extras/standardize_address_1 \
	$(topsrcdir)/regress/tests/extras/standardize_address_2 \

GISTESTS += $(topsrcdir)/regress/tests/test_global_end
############################################################

abstopsrcdir := $(realpath $(topsrcdir))
abssrcdir := $(realpath .)

GISTESTS := $(patsubst $(topsrcdir)/%,$(abstopsrcdir)/%,$(GISTESTS))
GISTESTS := $(patsubst $(abssrcdir)/%,./%,$(GISTESTS))

gischeck:

	@echo "GISRUNTESTFLAGS: $(GISRUNTESTFLAGS)"

	@$(PERL) $(topsrcdir)/regress/gis_run_test.pl $(GISRUNTESTFLAGS) $(GISTESTS)

# 暂不支持升级
#	@if echo "$(RUNTESTFLAGS)" | grep -vq -- --upgrade; then \
#		$(PERL) $(topsrcdir)/regress/run_test.pl \
#      --upgrade \
#      $(RUNTESTFLAGS) \
#      $(GISTESTS); \
#	fi

