# 产品名称：vGIS

## 产品简介
基于vastbase数据库的空间数据库，源于postgis3.2

## 支持插件
1）create extension postgis  
> postgis的基本核心功能，仅支持地理图形（矢量要素），这个是基础插件，其它的依赖此插件 
      
2）create extension postgis_raster  
> 对栅格数据的支持  
     
3）create extension postgis_sfcgal;   
> 主要是集成了CGAL（Computational Geometry Algorithms Library，计算几何算法库），来进行三维空间数据的空间运算，例如ST_3DDifference、ST_3DUnion 等，是通常空间运算在三维空间上的拓展。可产生各种3d模型，进而利用产生的数据进行3d渲染  
     
4）create extension postgis_topology;  
> 拓扑关系支持，可以对空间数据进行拓扑关系的检测和维护 
      
5）create extension fuzzystrmatch;  
> 是一个判断字符串之间相似性和距离的一个插件，postgis本身不包含fuzzystrmatch，     postgis_tiger_geocoder插件依赖于fuzzystrmatch插件  
     
6）create extension postgis_tiger_geocoder;   
> TIGER指的是（拓扑集成地理编码和参考），这个是美国人口普查局的GIS数据，提供了美国全国的行政区划、交通道路、水系等空间数据。这个扩展提供了TIGER数据的地理编码支持。注意，此扩展启用前，需要先启用fuzzystrmatch（字符串模糊查询）  
     
7）create extension address_standardizer;  
> TIGER数据地址规则化，是基于PAGC标准的地名标准化的扩展  
     
8）create extension address_standardizer_data_us;  
> TIGER地址规则化示例数据集，包含了基于PAGC标准的地名标准化的美国样例数据  

## 暂不支持特性  
1）由于不支持BRIN 索引而不支持的功能  
>  operator class	brin_geography_inclusion_ops  
	operator class	brin_geometry_inclusion_ops_2d  
	operator class	brin_geometry_inclusion_ops_3d  
	operator class	brin_geometry_inclusion_ops_4d  
     
2）由于不支持spgist 索引而不支持的功能  
>	operator class	spgist_geometry_ops_2d  
	operator class	spgist_geometry_ops_3d  
	operator class	spgist_geometry_ops_nd  
	operator class	spgist_geography_ops_nd  
     
3）SELECT不支持WITH ORDINALITY   
>	topology.ValidateTopology保持旧版功能  
      
4）不支持EXCLUDE constraint用法   
>	function	_add_raster_constraint_spatially_unique  
     
5）其它不支持   
>	function	ST_AsGeoJson(record,...)  
    