 /***
 *
 * Copyright (C) 2012 Regina Obe and Leo Hsu (Paragon Corporation)
 **/
-- Adds a schema to  the front of search path so that functions, tables etc get installed by default in set schema
-- but if people have postgis and other things installed in non-public, it will still keep those in path
-- Example usage: SELECT tiger.SetSearchPathForInstall('tiger');
CREATE OR REPLACE FUNCTION tiger.SetSearchPathForInstall(a_schema_name varchar)
RETURNS text
AS
$$
DECLARE
	var_result text;
	var_cur_search_path text;
BEGIN
	SHOW search_path INTO var_cur_search_path;

	EXECUTE 'SET search_path = ' || quote_ident(a_schema_name) || ', ' || var_cur_search_path;
	var_result := a_schema_name || ' has been made primary for install ';
  RETURN var_result;
END
$$
LANGUAGE 'plpgsql' VOLATILE STRICT;

CREATE OR REPLACE FUNCTION tiger._soundex(text) RETURNS text
AS
$$
DECLARE
	fsname text;
	_sql text;
	result text;
BEGIN
	-- 查找fuzzystrmatch中函数所在的schema名称
	select nspname into fsname from pg_namespace where oid = (select pronamespace from pg_proc where proname = 'levenshtein' limit 1);
	_sql := 'select ' || fsname || '.soundex(' || quote_literal($1) || ')';
	execute _sql into result;
	return result;
END;
$$
LANGUAGE plpgsql IMMUTABLE STRICT;
