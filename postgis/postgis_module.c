/**********************************************************************
 *
 * PostGIS - Spatial Types for PostgreSQL
 * http://postgis.net
 *
 * PostGIS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * PostGIS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PostGIS.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2011  OpenGeo.org
 *
 **********************************************************************/


#include "postgres.h"
#include "fmgr.h"
#include "utils/elog.h"
#include "utils/guc.h"
#include "libpq/pqsignal.h"

#include "../postgis_config.h"

#include "lwgeom_log.h"
#include "lwgeom_pg.h"
#include "geos_c.h"

#ifdef HAVE_LIBPROTOBUF
#include "lwgeom_wagyu.h"
#endif

/*
 * This is required for builds against pgsql
 */
PG_MODULE_MAGIC;

static gs_sigfunc coreIntHandler = 0;
static void handleInterrupt(int sig);

#ifdef WIN32
static void interruptCallback() {
  if (UNBLOCKED_SIGNAL_QUEUE())
    pgwin32_dispatch_queued_signals();
}
#endif

/*
 * Module load callback
 */
extern "C" void _PG_init(void);
void
_PG_init(void)
{
  gs_sigfunc oldHandler = coreIntHandler;
  coreIntHandler = gspqsignal(SIGINT, handleInterrupt);
  if (coreIntHandler == handleInterrupt)
    coreIntHandler = oldHandler;

#ifdef WIN32
  GEOS_interruptRegisterCallback(interruptCallback);
  lwgeom_register_interrupt_callback(interruptCallback);
#endif

  /* install PostgreSQL handlers */
  pg_install_lwgeom_handlers();
}

/*
 * Module unload callback
 */
void _PG_fini(void);
void
_PG_fini(void)
{
  elog(NOTICE, "Goodbye from PostGIS %s", POSTGIS_VERSION);
  pqsignal(SIGINT, coreIntHandler);
}

#if VASTBASE_IS_SUPPORTED

typedef struct postgis_session_context {
  lwgeompg_session_context* lsc;
} postgis_session_context;

extern "C" void set_extension_index(uint32 index);
extern "C" void init_session_vars(void);
extern "C" postgis_session_context* get_postgis_session_context();
extern "C" lwgeompg_session_context* get_postgis_lwgeompg_session_context();

uint32 postgis_index = -1;

/* 内核代码载入动态库时,先执行set_session_index再执行PG_init */
void set_extension_index(uint32 index)
{
  postgis_index = index;
}

void init_session_vars(void)
{
    RepallocSessionVarsArrayIfNecessary();

    postgis_session_context* psc =
        (postgis_session_context*)MemoryContextAllocZero(u_sess->self_mem_cxt, sizeof(postgis_session_context));
    u_sess->attr.attr_common.extension_session_vars_array[postgis_index] = psc;

    psc->lsc = NULL;
}

postgis_session_context* get_postgis_session_context()
{
  if (u_sess->attr.attr_common.extension_session_vars_array[postgis_index] == NULL) {
    init_session_vars();
  }

  postgis_session_context* psc = (postgis_session_context*)u_sess->attr.attr_common.extension_session_vars_array[postgis_index];
  return psc;
}

lwgeompg_session_context* get_postgis_lwgeompg_session_context() {
  postgis_session_context* psc = get_postgis_session_context();
  if(psc->lsc == NULL) {
    psc->lsc = (lwgeompg_session_context*)MemoryContextAllocZero(u_sess->self_mem_cxt, sizeof(lwgeompg_session_context));
    psc->lsc->postgis_constants = NULL;
    psc->lsc->proj_cache = NULL;
  }
  return psc->lsc;
}
#endif

static void
handleInterrupt(int sig)
{
  /* NOTE: printf here would be dangerous, see
   * https://trac.osgeo.org/postgis/ticket/3644
   *
   * TODO: block interrupts during execution, to fix the problem
   */
  /* printf("Interrupt requested\n"); fflush(stdout); */

  GEOS_interruptRequest();

#ifdef HAVE_LIBPROTOBUF
  lwgeom_wagyu_interruptRequest();
#endif

  /* request interruption of liblwgeom as well */
  lwgeom_request_interrupt();

  if ( coreIntHandler ) {
    (*coreIntHandler)(sig);
  }
}
