set client_min_messages to WARNING;

\i ../invalid_topology.sql

-- Validate full topology, store invalidities in a table
CREATE TABLE invalid_topology.invalidities AS
SELECT * FROM topology.validatetopology('invalid_topology');
SELECT * FROM invalid_topology.invalidities
ORDER BY 1,2,3;

-- clean up
SELECT topology.DropTopology('invalid_topology');
